//
// Created by Kirill on 10/28/19.
//

#define STRUCTNAME earth_cooling_state_lin

    ADD_ALG(earth_cooling_lin, STRUCTNAME,
            struct STRUCTNAME : public comp_math::state_itf {
                static const std::string name;
                double k = 1.0;
                double c = 1.0;
                double ro = 1.0;
                double sigma = 5.67 * pow(10, -8);
                double T0 = 1.0;
                double theta = 0.5;
                std::vector<double> alpha = std::vector<double>(int(x_end / dx) + 1
                COMMA 0.0);
                std::vector<double> beta = std::vector<double>(int(x_end / dx) + 1
                COMMA 0.0);


                using comp_math::state_itf::state_itf;

                std::string get_name() const override {
                    return STRUCTNAME::name;
                }

                p::dict get_state_dict() const override {
                    auto dict = state_itf::get_state_dict();
                    dict["k"] = k;
                    dict["T0"] = T0;
                    dict["c"] = c;
                    dict["ro"] = ro;
                    dict["sigma"] = sigma;
                    dict["theta"] = theta;
                    return dict;
                }

                void init(const p::dict &dict_state) override {
                    k = p::extract<double>(dict_state["k"]);
                    c = p::extract<double>(dict_state["c"]);
                    ro = p::extract<double>(dict_state["ro"]);
                    theta = p::extract<double>(dict_state["theta"]);
                    T0 = p::extract<double>(dict_state["T0"]);
//                    std::cout<<std::endl;
//                    std::cout<<"setting y:"<<std::endl;
                    state_itf::init(dict_state);
//                    std::cout<<std::endl;
                    alpha = std::vector<double>(x_point_number() + 1
                    COMMA
                    0.0);
                    beta = std::vector<double>(x_point_number() + 1
                    COMMA
                    0.0);
                    alpha[1] = 0.0;
                    beta[1] = T0;
                    for(int i = 0; i < beta.size(); ++i){
                        beta[i] = starting_conditions(i);
                    }
                }

                std::string help() override {
                    return state_itf::help() +
                           "k: " + std::to_string(k) + "\n" +
                           "c: " + std::to_string(c) + "\n" +
                           "ro: " + std::to_string(ro) + "\n" +
                           "sigma: " + std::to_string(sigma) + "\n" +
                           "theta: " + std::to_string(theta) + "\n" +
                           "T0: " + std::to_string(T0) + "\n";

                }

                std::vector<double> step() override {
//                    std::cout << y[y.size() - 1] << " - " << y[y.size() - 2] << " = "
//                              << y[y.size() - 1] - y[y.size() - 2] << std::endl;
//                     std::cout<<"y: ";
//                     for(const auto& y_i : y){
//                         std::cout<<y_i<<", ";
//                     }
//                     std::cout<<std::endl;
                    // std::cout<<"alpha: ";
                    // for(const auto& y_i : alpha){
                    //     std::cout<<y_i<<", ";
                    // }
                    // std::cout<<std::endl;
                    // std::cout<<"beta: ";
                    // for(const auto& y_i : beta){
                    //     std::cout<<y_i<<", ";
                    // }
                    // std::cout<<std::endl;
                    //std::cout<<"forward"<<std::endl;
                    for (int i = 2; i < x_point_number() + 1; ++i) {
                        auto result = forward_pass(int(i) - 1);
                        alpha[i] = result.first;
                        beta[i] = result.second;
                    }
                    //std::cout<<std::endl;
                    auto new_y = std::vector<double>(x_point_number(), 0.0);
                    for (int i = x_point_number() - 1; i >= 0; --i) {
                        new_y[i] = scheme(i + 1);
                    }
                    y = new_y;
                    ++it;
                    return new_y;
                }

                std::pair<double COMMA double> forward_pass(int idx) {
                    auto y_left = border_conditions(int(idx) - 1);
                    auto y_cur = border_conditions(int(idx));
                    auto y_right = border_conditions(int(idx) + 1);
                    auto A = theta * dt / (dx * dx);
                    auto B = A;
                    auto lambda = k / (c * ro);
                    auto C = 1.0 + 2.0 * theta * dt * lambda / (dx * dx);
                    auto D = (y_right - 2 * y_cur + y_left) * (dt * (1 - theta) * lambda) / (dx * dx) + y_cur;
                    auto alpha_next = B / (C - A * alpha[idx]);
                    auto beta_next = (A * beta[idx] + D) / (C - A * alpha[idx]);
                    // std::cout<<"("<<A<<", "<<B<<", "<<C<<", "<<D<<")\n";
                    // printf("%d: D(%f) = (%f - 2*%f + %f) * (%f * (1 - %f)*%f)/(%f*%f) + %f: dD = %f,\n"
                    //        " cur_beta: %f, next_beta: %f, dbeta: %f\n"
                    //        "next_beta = (%f*%f + %f)/(%f - %f*%f)\n",
                    //         idx, D, y_right, y_cur, y_left, dt, theta, lambda, dx,dx,y_cur, D - y_cur, beta[idx + 1],
                    //         beta_next, beta_next - beta[idx + 1], A, beta[idx], D, C, A, alpha[idx]);
                    return std::pair<double COMMA double>(alpha_next
                    COMMA
                    beta_next);
                }
            },
            [](int i COMMA const comp_math::state_itf *state) -> double {
                auto spec_state = dynamic_cast<const comp_math::STRUCTNAME *>(state);
                if (i == state->x_point_number()) {
                    auto alpha_M = spec_state->alpha[i];
                    auto beta_M = spec_state->beta[i];
                    auto T_prev = spec_state->y[i];
                    auto A = spec_state->dx * spec_state->sigma / spec_state->k * T_prev * T_prev * T_prev;
                    double T_M = beta_M / (A + 1 - alpha_M);
                    // std::cout<<"T_M: "<<T_M<<std::endl;
                    return T_M;
                }
                auto y_cur = spec_state->border_conditions(i);
                auto y_prev = spec_state->alpha[i] * y_cur + spec_state->beta[i];
                return y_prev;
            },
            [](int idx, const comp_math::state_itf *state) -> double {
                auto spec_state = dynamic_cast<const comp_math::STRUCTNAME *>(state);
                if (idx <= 0) {
                    return spec_state->T0;
                }
                if (idx >= state->x_point_number()) {
                    return 0.0;
                }
                return state->y[idx];
            },
            [](int idx, const comp_math::state_itf *state) -> double {
                auto spec_state = dynamic_cast<const comp_math::STRUCTNAME *>(state);
                auto L = spec_state->x_point_number() * spec_state->dx;
                auto x = idx * spec_state->dx;
                double T1 = 300;
                double T0 = spec_state->T0;
                double T = -(T0 - T1) / L * x + T0;
                return T;

            }
//            comp_math::default_descriptions::step_starting_conditions_factory(2 COMMA 300)
    )

#undef STRUCTNAME


#define STRUCTNAME earth_cooling_state_const

    ADD_ALG(earth_cooling_const, STRUCTNAME,
            struct STRUCTNAME : public comp_math::state_itf {
                static const std::string name;
                double k = 1.0;
                double c = 1.0;
                double ro = 1.0;
                double sigma = 5.67 * pow(10, -8);
                double T0 = 1.0;
                double theta = 0.5;
                std::vector<double> alpha = std::vector<double>(int(x_end / dx) + 1
                COMMA 0.0);
                std::vector<double> beta = std::vector<double>(int(x_end / dx) + 1
                COMMA 0.0);


                using comp_math::state_itf::state_itf;

                std::string get_name() const override {
                    return STRUCTNAME::name;
                }

                p::dict get_state_dict() const override {
                    auto dict = state_itf::get_state_dict();
                    dict["k"] = k;
                    dict["T0"] = T0;
                    dict["c"] = c;
                    dict["ro"] = ro;
                    dict["sigma"] = sigma;
                    dict["theta"] = theta;
                    return dict;
                }

                void init(const p::dict &dict_state) override {
                    k = p::extract<double>(dict_state["k"]);
                    c = p::extract<double>(dict_state["c"]);
                    ro = p::extract<double>(dict_state["ro"]);
                    theta = p::extract<double>(dict_state["theta"]);
                    T0 = p::extract<double>(dict_state["T0"]);
//                    std::cout<<std::endl;
//                    std::cout<<"setting y:"<<std::endl;
                    state_itf::init(dict_state);
//                    std::cout<<std::endl;
                    alpha = std::vector<double>(x_point_number() + 1
                    COMMA
                    0.0);
                    beta = std::vector<double>(x_point_number() + 1
                    COMMA
                    0.0);
                    alpha[1] = 0.0;
                    beta[1] = T0;
                    for(int i = 0; i < beta.size(); ++i){
                        beta[i] = starting_conditions(i);
                    }
                }

                std::string help() override {
                    return state_itf::help() +
                           "k: " + std::to_string(k) + "\n" +
                           "c: " + std::to_string(c) + "\n" +
                           "ro: " + std::to_string(ro) + "\n" +
                           "sigma: " + std::to_string(sigma) + "\n" +
                           "theta: " + std::to_string(theta) + "\n" +
                           "T0: " + std::to_string(T0) + "\n";

                }

                std::vector<double> step() override {
//                    std::cout << y[y.size() - 1] << " - " << y[y.size() - 2] << " = "
//                              << y[y.size() - 1] - y[y.size() - 2] << std::endl;
//                     std::cout<<"y: ";
//                     for(const auto& y_i : y){
//                         std::cout<<y_i<<", ";
//                     }
//                     std::cout<<std::endl;
                    // std::cout<<"alpha: ";
                    // for(const auto& y_i : alpha){
                    //     std::cout<<y_i<<", ";
                    // }
                    // std::cout<<std::endl;
                    // std::cout<<"beta: ";
                    // for(const auto& y_i : beta){
                    //     std::cout<<y_i<<", ";
                    // }
                    // std::cout<<std::endl;
                    //std::cout<<"forward"<<std::endl;
                    for (int i = 2; i < x_point_number() + 1; ++i) {
                        auto result = forward_pass(int(i) - 1);
                        alpha[i] = result.first;
                        beta[i] = result.second;
                    }
                    //std::cout<<std::endl;
                    auto new_y = std::vector<double>(x_point_number(), 0.0);
                    for (int i = x_point_number() - 1; i >= 0; --i) {
                        new_y[i] = scheme(i + 1);
                    }
                    y = new_y;
                    ++it;
                    return new_y;
                }

                std::pair<double COMMA double> forward_pass(int idx) {
                    auto y_left = border_conditions(int(idx) - 1);
                    auto y_cur = border_conditions(int(idx));
                    auto y_right = border_conditions(int(idx) + 1);
                    auto A = theta * dt / (dx * dx);
                    auto B = A;
                    auto lambda = k / (c * ro);
                    auto C = 1.0 + 2.0 * theta * dt * lambda / (dx * dx);
                    auto D = (y_right - 2 * y_cur + y_left) * (dt * (1 - theta) * lambda) / (dx * dx) + y_cur;
                    auto alpha_next = B / (C - A * alpha[idx]);
                    auto beta_next = (A * beta[idx] + D) / (C - A * alpha[idx]);
                    // std::cout<<"("<<A<<", "<<B<<", "<<C<<", "<<D<<")\n";
                    // printf("%d: D(%f) = (%f - 2*%f + %f) * (%f * (1 - %f)*%f)/(%f*%f) + %f: dD = %f,\n"
                    //        " cur_beta: %f, next_beta: %f, dbeta: %f\n"
                    //        "next_beta = (%f*%f + %f)/(%f - %f*%f)\n",
                    //         idx, D, y_right, y_cur, y_left, dt, theta, lambda, dx,dx,y_cur, D - y_cur, beta[idx + 1],
                    //         beta_next, beta_next - beta[idx + 1], A, beta[idx], D, C, A, alpha[idx]);
                    return std::pair<double COMMA double>(alpha_next
                    COMMA
                    beta_next);
                }
            },
            [](int i COMMA const comp_math::state_itf *state) -> double {
                auto spec_state = dynamic_cast<const comp_math::STRUCTNAME *>(state);
                if (i == state->x_point_number()) {
                    auto alpha_M = spec_state->alpha[i];
                    auto beta_M = spec_state->beta[i];
                    auto T_prev = spec_state->y[i];
                    auto A = spec_state->dx * spec_state->sigma / spec_state->k * T_prev * T_prev * T_prev * T_prev;
                    double T_M = (beta_M - A) / (1 - alpha_M);
                    // std::cout<<"T_M: "<<T_M<<std::endl;
                    return T_M;
                }
                auto y_cur = spec_state->border_conditions(i);
                auto y_prev = spec_state->alpha[i] * y_cur + spec_state->beta[i];
                return y_prev;
            },
            [](int idx, const comp_math::state_itf *state) -> double {
                auto spec_state = dynamic_cast<const comp_math::STRUCTNAME *>(state);
                if (idx <= 0) {
                    return spec_state->T0;
                }
                if (idx >= state->x_point_number()) {
                    return 0.0;
                }
                return state->y[idx];
            },
            [](int idx, const comp_math::state_itf *state) -> double {
                auto spec_state = dynamic_cast<const comp_math::STRUCTNAME *>(state);
                auto L = spec_state->x_point_number() * spec_state->dx;
                auto x = idx * spec_state->dx;
                double T1 = 300;
                double T0 = spec_state->T0;
                double T = -(T0 - T1) / L * x + T0;
                return T;

            }
//            comp_math::default_descriptions::step_starting_conditions_factory(2 COMMA 300)
    )

#undef STRUCTNAME
