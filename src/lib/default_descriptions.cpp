//
// Created by Kirill on 10/4/19.
//

/*
 * File with some useful functions to use as border and starting coditions.
 * Current list:
 *          starting conditions:
 *              - zero:
 *                  fills y_{0} with zeros
 *              - arbitrary step factory:
 *                  (double where, double height) -> std::function<double(int, state_itf)>
 *                  where: needed to be in (0,1) interval. Used to determine where step ends. (end_idx = int(N*where))
 *                  height: determines step's height.
 *           border conditions:
 *             - periodic condition factory: y_{-i} = y_{N - i} and y_{N + n} = y_{n + 1}
 *             - constant condition factory:
 *                   also works as 'same' condition factory due to implementation easyness.
 *                   (double left_const, double right_const, bool left_same, bool right_same) -> std::function<double(int, state_itf)>
 *                   if left_same then y_{-i} = y_{0}
 *                   if right_same then y_{N + i} = y_{N - 1}
 *                   if corresponding flag is false then:
 *                       y_{-i} = left_const
 *                       y_{N + i} = right_const
 * */

#include "../include/default_descriptions.h"

double comp_math::default_descriptions::periodic_border_conditions(int idx, const comp_math::state_itf *state) {
    auto old_idx = idx;
    if (idx < 0) {
        idx = -idx;
        idx = idx % int(state->x_point_number());
        idx = int(state->x_point_number()) - idx;
        return state->y[idx];
    }
    idx = idx % int(state->y[state->y.size()]);
    return state->y[idx];
}

double comp_math::default_descriptions::zero_starting_conditions(int idx, const comp_math::state_itf *state) {
    return 0;
}

comp_math::border_cond_type comp_math::default_descriptions::constant_border_conditions_factory(double a, double b,
                                                                                                bool left_same,
                                                                                                bool right_same) {
    return [a, b, left_same, right_same](int idx, const comp_math::state_itf *state) -> double {

        if (idx < 0) {
            return left_same ? state->y[0] : a;
        }
        if (idx >= state->y.size()) {
            return right_same ? state->y[state->y.size() - 1] : b;
        }
        return state->y[idx];
    };
}

comp_math::start_cond_type
comp_math::default_descriptions::step_starting_conditions_factory(double where, double hight) {
    return [where, hight](int idx, const comp_math::state_itf *state) -> double {
        if (idx < state->x_point_number() * where) {
            return hight;
        }
        return 0;
    };
}

double comp_math::default_descriptions::half_size_border_conditions(int idx, const state_itf *state){
    int x = idx%state->x_point_number();
    int y = idx/state->x_point_number();
    if(x == 0 and y < state->x_point_number()/2 and y >= 0){
        return 1;
    }
    if(idx < 0 or idx >= state->x_point_number()*state->x_point_number()){
        return 0.0;
    }
    return state->y[idx];
}


