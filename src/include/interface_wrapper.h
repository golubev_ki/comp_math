//
// Created by Kirill on 9/20/19.
//

/*
 * Description of the python interface. It can be said that it is just a wrapper of state
 * with some default method implementation. This class is wrapped in comp_math.cpp with boost
 * to be called through python
 * */

#ifndef COMP_MATH_INTERFACE_WRAPPER_H
#define COMP_MATH_INTERFACE_WRAPPER_H

#include "comp_alg_impl.h"
#include "default_descriptions.h"

namespace p = boost::python;
namespace np = boost::python::numpy;

namespace comp_math {

    class interface_wrapper {
        std::shared_ptr<state_itf> state_ptr;
        p::list res;
    public:
        void init(const p::dict &dict_state);

        [[nodiscard]] std::string help() const;

        np::ndarray step();

        [[nodiscard]] np::ndarray result() const;

        explicit interface_wrapper(const std::string &alg_name);

        interface_wrapper(const interface_wrapper &other);

        interface_wrapper(interface_wrapper &&other) noexcept;

        interface_wrapper &operator=(const interface_wrapper &other);

        interface_wrapper &operator=(interface_wrapper &&other) noexcept;

        [[nodiscard]] p::dict get_state_dict() const;

        [[nodiscard]] p::tuple get_internal_state() const;
        [[nodiscard]] p::tuple get_init_args() const;
        void set_internal_state(const p::tuple& int_state);


        p::list iterate(int n, int skip_it = 1);
    };
}


#endif //COMP_MATH_INTERFACE_WRAPPER_H
