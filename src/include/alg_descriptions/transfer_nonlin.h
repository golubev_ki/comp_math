//
// Created by Kirill on 10/27/19.
//


#define STRUCTNAME lax_ven_nonlin_state

    ADD_ALG(lax_ven_nonlinear, STRUCTNAME,
            struct STRUCTNAME : public comp_math::state_itf {
                static const std::string name;
                double a = 1.0;
                double q = 0.1;

                using comp_math::state_itf::state_itf;

                std::string get_name() const override {
                    return STRUCTNAME::name;
                }

                p::dict get_state_dict() const override {
                    auto dict = state_itf::get_state_dict();
                    dict["a"] = a;
                    dict["q"] = q;
                    return dict;
                }

                void init(const p::dict &dict_state) override {
                    state_itf::init(dict_state);
                    a = p::extract<double>(dict_state["a"]);
                    q = p::extract<double>(dict_state["q"]);
                }

                std::string help() override {
                    return state_itf::help() +
                           "a: " + std::to_string(a) + "\n" +
                           "q: " + std::to_string(q) + "\n";
                }

                std::vector<double> step() override {
                    auto y_res = state_itf::step();
                    auto y_res_smooth = y_res;
                    for (int i = 0; i < x_point_number(); ++i) {
                        auto D_mm = border_conditions(i - 1 ) - border_conditions(i - 2 );
                        auto D_m = border_conditions(i ) - border_conditions(i - 1 );
                        auto D_p = border_conditions(i + 1 ) - border_conditions(i );
                        auto D_pp = border_conditions(i + 2 ) - border_conditions(i + 1 );
                        auto Q_plus = 0.0;
                        if (D_pp * D_p < 0 or D_p * D_m < 0) {
                            Q_plus = D_p;
                        }
                        auto Q_minus = 0.0;
                        if (D_m * D_p < 0 or D_mm * D_m < 0) {
                            Q_minus = D_m;
                        }
                        y_res_smooth[i] = y_res[i] + q * (Q_plus - Q_minus);
                    }
                    y = y_res_smooth;
                    return y_res_smooth;
                }

                double scheme(int i) const override {
                    auto y_left = border_conditions(i - 1);
                    auto y_right = border_conditions(i + 1);
                    auto y_cent = border_conditions(i);

                    auto U_left = y_left * y_left / 2;
                    auto U_right = y_right * y_right / 2;
                    auto U_cent = y_cent * y_cent / 2;

                    auto Courant_num = a * dt / dx;
                    auto y_plus = 0.5 * (y_right + y_cent) - 0.5 * Courant_num * (U_right - U_cent);
                    auto y_minus = 0.5 * (y_cent + y_left) - 0.5 * Courant_num * (U_cent - U_left);

                    auto U_plus = y_plus * y_plus / 2;
                    auto U_minus = y_minus * y_minus / 2;

                    auto y_next = y_cent - Courant_num * (U_plus - U_minus);

                    return y_next;
                }

                double starting_conditions(int idx) const override {
                    return comp_math::default_descriptions::step_starting_conditions_factory(0.0, 1.0)(idx, this);
                }

                double border_conditions(int idx) const override {
                    return comp_math::default_descriptions::constant_border_conditions_factory(1.0, 0.0,
                                                                                               false,
                                                                                               true)(idx, this);
                }

                virtual p::tuple get_internal_state() const override{
                    auto state = state_itf::get_internal_state();
                    auto addition = p::make_tuple(a, q);
                    return p::tuple(p::list(state) + p::list(addition));
                }

                virtual void set_internal_state(const p::tuple& int_state) override{
                    state_itf::set_internal_state(int_state);
                    a = p::extract<double>(int_state[default_state_len]);
                    q = p::extract<double>(int_state[default_state_len + 1]);
                }
            }
    )

#undef STRUCTNAME

#define STRUCTNAME lax_nonlin_state

    ADD_ALG(lax_nonlinear, STRUCTNAME,
            struct STRUCTNAME : public comp_math::state_itf {
                static const std::string name;
                double a = 1.0;

                using comp_math::state_itf::state_itf;

                std::string get_name() const override {
                    return STRUCTNAME::name;
                }

                p::dict get_state_dict() const override {
                    auto dict = state_itf::get_state_dict();
                    dict["a"] = a;
                    return dict;
                }

                void init(const p::dict &dict_state) override {
                    state_itf::init(dict_state);
                    a = p::extract<double>(dict_state["a"]);
                }

                std::string help() override {
                    return state_itf::help() + "a: " + std::to_string(a) + "\n";
                }
                double scheme(int i) const override {
                    auto y_left =  border_conditions(i - 1);
                    auto U_left = y_left * y_left / 2;
                    auto y_right =  border_conditions(i + 1);
                    auto U_right = y_right * y_right / 2;
                    auto y_cent =  border_conditions(i);
                    auto Courant_num = a *  dt / ( dx);
                    auto y_next = 0.5 * (y_right + y_left) - 0.5 * Courant_num * (U_right - U_left);
                    return y_next;
                }

                double starting_conditions(int idx) const override {
                    return comp_math::default_descriptions::step_starting_conditions_factory(0.0, 1.0)(idx, this);
                }

                double border_conditions(int idx) const override {
                    return comp_math::default_descriptions::constant_border_conditions_factory(1.0, 0.0,
                                                                                               false,
                                                                                               true)(idx, this);
                }

                virtual p::tuple get_internal_state() const override{
                    auto state = state_itf::get_internal_state();
                    auto addition = p::make_tuple(a);
                    return p::tuple(p::list(state) + p::list(addition));
                }

                virtual void set_internal_state(const p::tuple& int_state) override{
                    state_itf::set_internal_state(int_state);
                    a = p::extract<double>(int_state[default_state_len]);
                }
            }
    )

#undef STRUCTNAME

#define STRUCTNAME analitic_solution_transfer_nonlin_state

    ADD_ALG(analitic_solution_transfer_nonlin, STRUCTNAME,
            struct STRUCTNAME : public comp_math::state_itf {
                static const std::string name;
                double a;

                using comp_math::state_itf::state_itf;

                std::string get_name() const override {
                    return STRUCTNAME::name;
                }

                p::dict get_state_dict() const override {
                    auto dict = state_itf::get_state_dict();
                    dict["a"] = a;
                    return dict;
                }

                void init(const p::dict &dict_state) override {
                    state_itf::init(dict_state);
                    a = p::extract<double>(dict_state["a"]);
                }

                std::string help() override {
                    return state_itf::help() + "a: " + std::to_string(a) + "\n";
                }

                virtual p::tuple get_internal_state() const override{
                    auto state = state_itf::get_internal_state();
                    auto addition = p::make_tuple(a);
                    return p::tuple(p::list(state) + p::list(addition));
                }

                virtual void set_internal_state(const p::tuple& int_state) override{
                    state_itf::set_internal_state(int_state);
                    a = p::extract<double>(int_state[default_state_len]);
                }

                double scheme(int i) const override {
                    auto x = dx * double(i);
                    auto t = dt * double(it);
                    auto y_next = 0.0;
                    if (x - a / 2.0 * t < 0.0) {
                        y_next = 1.0;
                    }
                    return y_next;
                }

                double starting_conditions(int idx) const override {
                    return comp_math::default_descriptions::step_starting_conditions_factory(0.0, 1.0)(idx, this);
                }

                double border_conditions(int idx) const override {
                    return comp_math::default_descriptions::constant_border_conditions_factory(1.0, 0.0,
                                                                                               false,
                                                                                               true)(idx, this);
                }

            }
    )

#undef STRUCTNAME
