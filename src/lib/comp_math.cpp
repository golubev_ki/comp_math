//
// Created by Kirill on 9/27/19.
//

/*
 * This is file for boost wrapper code. Nothing special.
 * todo: add reset method
 * */
# include "../include/interface_wrapper.h"


struct engine_pickle_suite : p::pickle_suite {
    static
    p::tuple getinitargs(const comp_math::interface_wrapper &engine) {
        return engine.get_init_args();
    }

    static
    p::tuple getstate(const comp_math::interface_wrapper &engine) {
        return engine.get_internal_state();
    }

    static
    void setstate(comp_math::interface_wrapper &engine, p::tuple state) {
        return engine.set_internal_state(state);
    }
};


BOOST_PYTHON_MODULE (comp_math) {
    Py_Initialize();
    np::initialize();
    p::class_<comp_math::interface_wrapper>("Comp_engine", p::init<const std::string &>())
            .def("help", &comp_math::interface_wrapper::help)
            .def("init", &comp_math::interface_wrapper::init)
            .def("step", &comp_math::interface_wrapper::step)
            .def("result", &comp_math::interface_wrapper::result)
            .def("iterate", &comp_math::interface_wrapper::iterate)
            .def("get_state", &comp_math::interface_wrapper::get_state_dict)
            .def_pickle(engine_pickle_suite());
}