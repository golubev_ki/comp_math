What is going on here?
====================
This is project for my computational math classes.\
It contains engine c++ source code which is wrapped with python interface. \
The most convenient way for me to wrap c++ class with python is [boost](https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/index.html). \
This is CMake project and so far boost paths are hardcoded. \
There is also [Eigen](https://eigen.tuxfamily.org/index.php?title=Main_Page) needed for some polynomial equation solving and \
implicit calculation scheme implementation.

What is available so far?
====================
The goal of this project to make as little as possible work\
when it is time to add new computational scheme\
I choose to apply some code generation with macroses and\
I think it is going fine so far.\
The main idea is to expand macroses differently in different context \
So the process of expanding the engine list is as easy as to add a new macros. \
The project structure is following:\

* CMakeList.txt
+ Readme.md 
- .gitignore 
* src/
    * include/
        + comp_alg_descriptions.h
        + comp_alg_implementations.h
        + default_descriptions.h
        + interface.h
        + interface_wrapper.h
        + alg_descriptions/ 
            + earth_cooling.h
            + termocund_lin.h
            + termocund_nonlin.h
            + transfer_lin.h
            + transfer_nonlin.h
    *  lib/
        + comp_alg_impl.cpp
        + comp_math.cpp
        + default_descritions.cpp
        + interface.cpp
        + interface_wrapper.cpp
        
 
The most interesting part is the alg_descriptions directory.\
There are files with macros calls but without macros definitions. \
This is on purpose. As it said earlier\
the idea is to expand them differently in different contexts. \
The macros I am talking about is ADD_ALG:\
    ADD_ALG(engine_name, structname, struct) 
    
    * engine_name - the name with which you will mention your algorithm(engine) when creating it in python.
    * structname - the name of the struct in next argument, for simplification reasons it is needed explicitly.
    * struct - the preferable way of providing it is to inherit it from comp_math::state_itf and then override methods you need to override and add data you want to add.   

Ok, it's cool. How to use it?
=====================

This is a tricky part. This is just a c++ module to call from python. \
And boost paths are hardcoded. \
So if you don't want to spend an hour setting things up it is not your choice. \
\
For those who are crazy enough, there is a guide: \
1. Install boost-python and run some hello_world thing to test the installation.\
2. find_package(Boost REQUIRED) works fine? Then delete target_link_libraries 
with hardcoded paths. If not, then find lib files yourself and link them the hard way. \
3.  This was a hard part. Now you need to:
    * mkdir build && cd build && cmake .. && make \
4. Compiled fine? Miracle it is. No? Then you are on your own. Sorry( \
5. Now you have fine shared library in example/comp_math directory.\
6. Run crawler_test.py and see if it's working fine.
Then look through data_crawler.py to see how to use one.

This shared library has only one thing in it and thing is Comp_engine:

    from comp_math import Comp_engine  
    first_try = Comp_engine("engine_name")
    first_try.init({"param1": 1, "param2": 2, ...})
    print(first_try.help())
There are few other methods of this class:
    
    help() - returns string with some useful info
    init(dict) - you MUST call it before any meaningful thing will be done.
           Arguments are params you specified for your algorithm which are passed as a dictionary. See some ADD_ALG for example.
    step() - computes next time layer of your scheme. returns result as numpy array
    iterate(int) - computes any given number of steps. Returns list of numpy arrays.
    result() - returns the current time layer of your algorithm.
    get_state() - get hyperparameters as dict, which you passed in init and some more.

One of the things you can't do yet is to have a deep copy of comp_engine. 

Can I see some examples?
========================
For now there is no python code here. So you can't just clone and run something.
But if you brave enough to look at sources you can start with alg_descriptions directory:\ 

* earth_cooling is a file, describing earth surface's temperature dynamics\
 in the situation when the sun is stolen by aliens. (currently does not work at all)
 
* termocund_lin is model of temperature propagating through a material with constant termoconductive coefficient. There are two schemes:
    - usual explicit scheme
    - three-point scheme (implicit)
    
* termocund_nonlin is model of temperature propagating through a material with non-constant termoconductive coefficient. There are two schemes:
    - usual explicit scheme
    - three-point scheme (implicit)
    
* transfer_lin is model of usual waves propagating through an environment
    - lax scheme (explicit)
    - lax & vender scheme (implicit)

* transfer_lin is model of shock waves propagating through the environment
    - lax scheme (explicit)
    - lax & vender scheme (implicit)
    
Important notes
===================
1. Each engine in the example executes in separate thread

What's next?
===================
1. Add some visual demo
2. Make engines copyable.
3. Some python demo?
4. ??
