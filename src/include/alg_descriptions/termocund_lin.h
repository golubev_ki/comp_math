//
// Created by Kirill on 10/27/19.
//

#define STRUCTNAME termocunductive_explicit_lin_state

    ADD_ALG(termocound_expl_lin, STRUCTNAME,
            struct STRUCTNAME : public comp_math::state_itf {
                static const std::string name;
                double a = 1.0;
                double T0 = 1.0;

                using comp_math::state_itf::state_itf;

                std::string get_name() const override {
                    return STRUCTNAME::name;
                }

                p::dict get_state_dict() const override {
                    auto dict = state_itf::get_state_dict();
                    dict["a"] = a;
                    dict["T0"] = T0;
                    return dict;
                }

                void init(const p::dict &dict_state) override {
                    state_itf::init(dict_state);
                    a = p::extract<double>(dict_state["a"]);
                    T0 = p::extract<double>(dict_state["T0"]);
                }

                std::string help() override {
                    return state_itf::help() + "a: " +
                           std::to_string(a) + "\n" + "T0: " +
                           std::to_string(T0) + "\n";

                }

                p::tuple get_internal_state() const override {
                    auto internal_state = p::make_tuple(a, T0);
                    return p::tuple(p::list(state_itf::get_internal_state()) + p::list(internal_state));
                }

                p::tuple get_init_args() const override {
                    return state_itf::get_init_args();
                }

                void set_internal_state(const p::tuple &int_state) override {
                    state_itf::set_internal_state(int_state);
                    a = p::extract<double>(int_state[default_state_len]);
                    T0 = p::extract<double>(int_state[default_state_len + 1]);
                }
                double scheme(int i) const override {
                    auto lambda =   a;
                    auto y_left =   border_conditions(int(i) - 1);
                    auto y_right =   border_conditions(int(i) + 1);
                    auto y_cent =   border_conditions(int(i));
                    auto Courant_num_p = lambda *   dt / (  dx *   dx);
                    auto y_next = y_cent + Courant_num_p * (y_right - 2 * y_cent + y_left);
                    return y_next;
                }

                double starting_conditions(int idx) const override {
                    return comp_math::default_descriptions::zero_starting_conditions(idx, this);
                }

                double border_conditions(int idx) const override {
                    if (idx < 0) {
                
                        return T0;
                    }
                    if (idx >= x_point_number()) {
                        return 0.0;
                    }
                    return y[idx];
                }
            })

#undef STRUCTNAME

#define STRUCTNAME termocunductive_implicit_lin_state

    ADD_ALG(termocound_impl_lin, STRUCTNAME,
            struct STRUCTNAME : public comp_math::state_itf {
                static const std::string name;
                double a = 1.0;
                double T0 = 1.0;
                double sigma0 = 1.0;
                double theta = 0.5;
                std::vector<double> alpha = std::vector<double>(int(x_end/dx) + 1 COMMA 0.0);
                std::vector<double> beta = std::vector<double>(int(x_end/dx) + 1 COMMA 0.0);

                using comp_math::state_itf::state_itf;

                std::string get_name() const override {
                    return STRUCTNAME::name;
                }

                p::dict get_state_dict() const override {
                    auto dict = state_itf::get_state_dict();
                    dict["a"] = a;
                    dict["T0"] = T0;
                    dict["sigma0"] = sigma0;
                    dict["theta"] = theta;
                    return dict;
                }

                void init(const p::dict &dict_state) override {
                    state_itf::init(dict_state);
                    a = p::extract<double>(dict_state["a"]);
                    T0 = p::extract<double>(dict_state["T0"]);
                    sigma0 = p::extract<double>(dict_state["sigma0"]);
                    theta = p::extract<double>(dict_state["theta"]);
                    alpha = std::vector<double>(x_point_number() + 1 COMMA 0.0);
                    beta = std::vector<double>(x_point_number() + 1 COMMA 0.0);
                    alpha[1] = 0.0;
                    beta[1] = T0;
                }

                std::string help() override {
                    return state_itf::help() +
                           "a: " + std::to_string(a) + "\n" +
                           "T0: " + std::to_string(T0) + "\n" +
                           "sigma0: " + std::to_string(sigma0) + "\n" +
                           "theta:" + std::to_string(theta) + "\n";
                }

                std::vector<double> step() override {
                    for (int i = 2; i < x_point_number() + 1; ++i) {
                        auto result = forward_pass(int(i) - 1);
                        alpha[i] = result.first;
                        beta[i] = result.second;
                    }
                    auto new_y = std::vector<double>(x_point_number(), 0.0);
                    for (int i = x_point_number() - 1; i >= 0; --i) {
                        new_y[i] = scheme(i + 1  );
                    }
                    y = new_y;
                    ++it;
                    return new_y;
                }

                std::pair<double COMMA double> forward_pass(int idx) {
                    auto y_left = border_conditions(int(idx) - 1);
                    auto y_cur = border_conditions(int(idx));
                    auto y_right = border_conditions(int(idx) + 1);
                    auto A = theta * dt / (dx * dx);
                    auto B = A;
                    auto lambda = a;
                    auto C = 1.0 + 2.0 * theta * dt * lambda / (dx * dx);
                    auto D = (y_right - 2 * y_cur + y_left) * (dt * (1 - theta) * lambda) / (dx * dx) + y_cur;
                    auto alpha_next = B / (C - A * alpha[idx]);
                    auto beta_next = (A * beta[idx] + D) / (C - A * alpha[idx]);
                    return std::pair<double COMMA double>(alpha_next COMMA beta_next);
                }

                p::tuple get_internal_state() const override {
                    auto internal_state = p::make_tuple(a, T0, sigma0, theta,
                                                        to_numpy(alpha),
                                                        to_numpy(beta));
                    return p::tuple(p::list(state_itf::get_internal_state()) + p::list(internal_state));
                }

                p::tuple get_init_args() const override {
                    return state_itf::get_init_args();
                }

                void set_internal_state(const p::tuple &int_state) override {
                    state_itf::set_internal_state(int_state);
                    a = p::extract<double>(int_state[default_state_len]);
                    T0 = p::extract<double>(int_state[default_state_len + 1]);
                    sigma0 = p::extract<double>(int_state[default_state_len + 2]);
                    theta = p::extract<double>(int_state[default_state_len + 3]);
                    alpha = from_numpy(p::extract<np::ndarray>(int_state[default_state_len + 4]));
                    beta = from_numpy(p::extract<np::ndarray>(int_state[default_state_len + 5]));
                }

                double scheme(int i) const override {
                    auto y_cur = border_conditions(i);
                    auto y_prev = alpha[i] * y_cur + beta[i];
                    return y_prev;
                }

                double starting_conditions(int idx) const override {
                    return comp_math::default_descriptions::zero_starting_conditions(idx, this);
                }

                double border_conditions(int idx) const override {
                    if (idx < 0) {
                    return T0;
                    }
                    if (idx >= x_point_number()) {
                        return 0.0;
                    }
                    return y[idx];
                }
            })


#undef STRUCTNAME


#define STRUCTNAME termocunductive_lin_analitic_solution_state

    ADD_ALG(termocound_linear_analitic_solution, STRUCTNAME,
            struct STRUCTNAME : public comp_math::state_itf {
                static const std::string name;
                double a = 1.0;
                double T0 = 1.0;
                double sigma0 = 1.0;

                using comp_math::state_itf::state_itf;

                std::string get_name() const override {
                    return STRUCTNAME::name;
                }

                p::dict get_state_dict() const override {
                    auto dict = state_itf::get_state_dict();
                    dict["a"] = a;
                    dict["T0"] = T0;
                    dict["sigma0"] = sigma0;
                    return dict;
                }

                void init(const p::dict &dict_state) override {
                    state_itf::init(dict_state);
                    a = p::extract<double>(dict_state["a"]);
                    T0 = p::extract<double>(dict_state["T0"]);
                    sigma0 = p::extract<double>(dict_state["sigma0"]);
                }

                std::string help() override {
                    return state_itf::help() +
                           "a: " + std::to_string(a) + "\n" +
                           "T0: " + std::to_string(T0) + "\n" +
                           "sigma0: " + std::to_string(sigma0) + "\n";

                }

                p::tuple get_internal_state() const override {
                    auto internal_state = p::make_tuple(a, T0, sigma0);
                    return p::tuple(p::list(state_itf::get_internal_state()) + p::list(internal_state));
                }

                p::tuple get_init_args() const override {
                    return state_itf::get_init_args();
                }

                void set_internal_state(const p::tuple &int_state) override {
                    state_itf::set_internal_state(int_state);
                    a = p::extract<double>(int_state[default_state_len]);
                    T0 = p::extract<double>(int_state[default_state_len + 1]);
                    sigma0 = p::extract<double>(int_state[default_state_len  +2]);
                }

                double scheme(int i) const override {
                    auto x = dx * i;
                    auto t = dt * it;
                    auto lambda = a;
                    auto y_next = T0 *
                              //this solution is satisfying border conditions better then original one
                              //(1 - erf(x / (2 * sqrt(lambda * t))) / erf(  x_end / (2 * sqrt(lambda * t))));
                              //this is original solution
                              erfc(x / (2 * sqrt(lambda * t)));
                    return y_next;
                }

                double starting_conditions(int idx) const override {
                    return comp_math::default_descriptions::zero_starting_conditions(idx, this);
                }

                double border_conditions(int idx) const override {
                    if (idx < 0) {
                    return T0;
                    }
                    if (idx >= x_point_number()) {
                        return 0.0;
                    }
                    return y[idx];
                }

            })


#undef STRUCTNAME
