//
// Created by Kirill on 2019-09-19.
//

/*
 * Description off interface which is used for computational engine's state.
 * Abstract methods:
 *      - std::string get_name():
 *          used for returning a name of the algorithm. Usually it needs to be static const std::string
 *          and defined in comp_alg_description.h
 *      - std::unique_ptr<state_itf> copy():
 *          used to define copy behaviour of state
 *
 * */

#ifndef COMP_MATH_INTERFACE_H
#define COMP_MATH_INTERFACE_H


#include <boost/python.hpp>
#include <boost/python/numpy.hpp>
#include <memory>
#include <iostream>
#include <functional>
#include <string>
#include <unistd.h>
#include <stdio.h>
#include <Eigen/Sparse>
#include <iomanip>

#define COMMA ,

using std::cout, std::endl;
namespace p = boost::python;
namespace np = boost::python::numpy;

namespace comp_math {
    struct state_itf;
    typedef std::unique_ptr<state_itf> state_itf_ptr_type;
    typedef std::function<double(int, const state_itf *)> border_cond_type;
    typedef std::function<double(int, const state_itf *)> start_cond_type;
    typedef std::function<double(int, const state_itf *)> scheme_type;

    struct state_itf {
        std::vector<double> y;
        int it;
        double dx, dt, x_end;
        int default_state_len = 0;

        state_itf();

        virtual double border_conditions(int idx) const = 0;

        virtual double starting_conditions(int idx) const = 0;

        virtual double scheme(int idx) const = 0;

        virtual std::vector<double> step();

        virtual ~state_itf() = default;

        virtual void init(const p::dict &dict_state);

        virtual std::string help();

        virtual p::tuple get_init_args() const;

        virtual p::tuple get_internal_state() const;

        virtual void set_internal_state(const p::tuple& int_state);

        [[nodiscard]] virtual p::dict get_state_dict() const;

        [[nodiscard]] int x_point_number() const;

        virtual void set_starting_conditions();

        [[nodiscard]] virtual std::string get_name() const = 0;

        static std::vector<double> from_numpy(const np::ndarray &arr);

        static np::ndarray to_numpy(const std::vector<double> &vec);

        static p::list to_list(const std::vector<double> & vec);

        static std::vector<double> to_vector(const p::list &l);

        template<class R, class ... Args>
        static p::object to_python(std::function<R(Args ...)> func) {
            auto call_policies = boost::python::default_call_policies();
            typedef boost::mpl::vector<R, Args ...> func_sig;
            return p::make_function(func, call_policies, func_sig());
        }

        template<class R, class ... Args>
        static std::function<R(Args...)> from_python(const p::object &obj) {
            return [obj](Args ... args) -> R {
                return p::extract<R>(obj(std::forward<Args...>(args...)));
            };
        }
    };
}
#endif //COMP_MATH_INTERFACE_H
