//
// Created by Kirill on 10/27/19.
//

//---------------------------------------------------------------------------------------------------

#define STRUCTNAME termocunductive_nonlin_analitic_solution_state

    ADD_ALG(termocound_nonlinear_analitic_solution, STRUCTNAME,
            struct STRUCTNAME : public comp_math::state_itf {
                static const std::string name;
                double a = 1.0;
                double T0 = 1.0;
                double sigma0 = 1.0;

                using comp_math::state_itf::state_itf;

                std::string get_name() const override {
                    return STRUCTNAME::name;
                }

                p::dict get_state_dict() const override {
                    auto dict = state_itf::get_state_dict();
                    dict["a"] = a;
                    dict["T0"] = T0;
                    dict["sigma0"] = sigma0;
                    return dict;
                }

                void init(const p::dict &dict_state) override {
                    state_itf::init(dict_state);
                    a = p::extract<double>(dict_state["a"]);
                    T0 = p::extract<double>(dict_state["T0"]);
                    sigma0 = p::extract<double>(dict_state["sigma0"]);
                }

                std::string help() override {
                    return state_itf::help() +
                           "a: " + std::to_string(a) + "\n" +
                           "T0: " + std::to_string(T0) + "\n" +
                           "sigma0: " + std::to_string(sigma0) + "\n";

                }

                p::tuple get_internal_state() const override {
                    auto internal_state = p::make_tuple(a, T0, sigma0);
                    return p::tuple(p::list(state_itf::get_internal_state()) + p::list(internal_state));
                }

                p::tuple get_init_args() const override {
                    return state_itf::get_init_args();
                }

                void set_internal_state(const p::tuple &int_state) override {
                    state_itf::set_internal_state(int_state);
                    a = p::extract<double>(int_state[default_state_len]);
                    T0 = p::extract<double>(int_state[default_state_len + 1]);
                    sigma0 = p::extract<double>(int_state[default_state_len + 2]);
                }

                double scheme(int i) const override {
                    auto x = dx * i;
                    auto t = dt * it;
                    auto lambda = a;
                    auto sigma = sigma0;
                    auto c = sqrt(pow(T0, sigma) * lambda / sigma);
                    if (x - c * t <= 0) {
                        return pow(sigma * c / lambda * (c * t - x), 1.0 / sigma);
                    }
                    return 0.0;
                }

                double starting_conditions(int idx) const override {
                    return comp_math::default_descriptions::zero_starting_conditions(idx, this);
                }

                double border_conditions(int idx) const override {
                    if (idx < 0) {
                        auto t = it * dt;
                        return T0 * pow(t, 1.0 / sigma0);
                    }
                    if (idx >= x_point_number()) {
                        return 0.0;
                    }
                    return y[idx];
                }


            })


#undef STRUCTNAME


#define STRUCTNAME termocunductive_explicit_nonlin_state

    ADD_ALG(termocound_expl_nonlin, STRUCTNAME,
            struct STRUCTNAME : public comp_math::state_itf {
                static const std::string name;
                double a = 1.0;
                double T0 = 1.0;
                double sigma0 = 1.0;

                using comp_math::state_itf::state_itf;

                std::string get_name() const override {
                    return STRUCTNAME::name;
                }

                p::dict get_state_dict() const override {
                    auto dict = state_itf::get_state_dict();
                    dict["a"] = a;
                    dict["T0"] = T0;
                    dict["sigma0"] = sigma0;
                    return dict;
                }

                void init(const p::dict &dict_state) override {
                    state_itf::init(dict_state);
                    a = p::extract<double>(dict_state["a"]);
                    T0 = p::extract<double>(dict_state["T0"]);
                    sigma0 = p::extract<double>(dict_state["sigma0"]);
                }

                p::tuple get_internal_state() const override {
                    auto internal_state = p::make_tuple(a, T0, sigma0);
                    return p::tuple(p::list(state_itf::get_internal_state()) + p::list(internal_state));
                }

                p::tuple get_init_args() const override {
                    return state_itf::get_init_args();
                }

                void set_internal_state(const p::tuple &int_state) override {
                    state_itf::set_internal_state(int_state);
                    a = p::extract<double>(int_state[default_state_len]);
                    T0 = p::extract<double>(int_state[default_state_len + 1]);
                    sigma0 = p::extract<double>(int_state[default_state_len + 2]);
                }

                std::string help() override {
                    return state_itf::help() +
                           "a: " + std::to_string(a) + "\n" +
                           "T0: " + std::to_string(T0) + "\n" +
                           "sigma0: " + std::to_string(sigma0) + "\n";
                }

                double scheme(int i) const override {
                    auto lambda0 = a;
                    auto y_left = border_conditions(i - 1);
                    auto y_right = border_conditions(i + 1 );
                    auto y_cent = border_conditions(i);
                    auto lambda_cent = lambda0 * pow(y_cent, sigma0);
                    auto lambda_left = lambda0 * pow(y_left, sigma0);
                    auto lambda_right = lambda0 * pow(y_right, sigma0);
                    auto lambda_plus = 0.5 * (lambda_cent + lambda_right);
                    auto lambda_minus = 0.5 * (lambda_cent + lambda_left);
                    auto Courant_num_p = dt / (dx * dx);
                    auto y_next =
                            y_cent + Courant_num_p * (lambda_plus * (y_right - y_cent) - lambda_minus * (y_cent - y_left));
                    return y_next;
                }

                double starting_conditions(int idx) const override {
                    return comp_math::default_descriptions::zero_starting_conditions(idx, this);
                }

                double border_conditions(int idx) const override {
                    if (idx < 0) {
                        auto t = it * dt;
                         
                        return T0 * pow(t, 1.0 / sigma0);
                    }
                    if (idx >= x_point_number()) {
                        return 0.0;
                    }
                    return y[idx];
                }
            })


#undef STRUCTNAME


#define STRUCTNAME termocunductive_implicit_nonlin_state

    ADD_ALG(termocound_impl_nonlin, STRUCTNAME,
            struct STRUCTNAME : public comp_math::state_itf {
                static const std::string name;
                double a = 1.0;
                double T0 = 1.0;
                double sigma0 = 1.0;
                double theta = 0.7;
                std::vector<double> alpha = std::vector<double>(int(x_end / dx) + 1
                COMMA 0.0);
                std::vector<double> beta = std::vector<double>(int(x_end / dx) + 1
                COMMA 0.0);
                std::vector<double> lambda_prev = std::vector<double>(int(x_end / dx)
                COMMA 0.0);

                using comp_math::state_itf::state_itf;

                std::string get_name() const override {
                    return STRUCTNAME::name;
                }

                p::dict get_state_dict() const override {
                    auto dict = state_itf::get_state_dict();
                    dict["a"] = a;
                    dict["T0"] = T0;
                    dict["sigma0"] = sigma0;
                    dict["theta"] = theta;
                    return dict;
                }

                void init(const p::dict &dict_state) override {
                    state_itf::init(dict_state);
                    a = p::extract<double>(dict_state["a"]);
                    T0 = p::extract<double>(dict_state["T0"]);
                    sigma0 = p::extract<double>(dict_state["sigma0"]);
                    theta = p::extract<double>(dict_state["theta"]);
                    alpha = std::vector<double>(x_point_number() + 1, 0.0);
                    beta = std::vector<double>(x_point_number() + 1, 0.0);
                    lambda_prev = std::vector<double>(x_point_number(), 0.0);
                }

                std::string help() override {
                    return state_itf::help() +
                           "a: " + std::to_string(a) + "\n" +
                           "T0: " + std::to_string(T0) + "\n" +
                           "sigma0: " + std::to_string(sigma0) + "\n" +
                           "theta:" + std::to_string(theta) + "\n";

                }

                std::vector<double> step() override {
//                    std::cout<<"y: ";
//                    for(const auto& y_i : y){
//                        std::cout<<y_i<<", ";
//                    }
//                    std::cout<<std::endl;
//                    std::cout<<"alpha: ";
//                    for(const auto& y_i : alpha){
//                        std::cout<<y_i<<", ";
//                    }
//                    std::cout<<std::endl;
//                    std::cout<<"beta: ";
//                    for(const auto& y_i : beta){
//                        std::cout<<y_i<<", ";
//                    }
//                    std::cout<<std::endl;
//                    usleep(1);
                    alpha[1] = 0.0;
                    beta[1] = border_conditions(-1 );
                    for (int i = 2; i < x_point_number() + 1; ++i) {
                        auto result = forward_pass(int(i) - 1);
                        alpha[i] = result.first;
                        beta[i] = result.second;
                    }
                    auto new_y = std::vector<double>(x_point_number(), 0.0);
                    for (int i = x_point_number() - 1; i >= 0; --i) {
                        new_y[i] = scheme(i + 1);
                    }
                    for (int i = 0; i < x_point_number(); ++i) {
                        lambda_prev[i] = a * pow(y[i] COMMA sigma0);
                    }
                    y = new_y;
                    ++it;
                    return new_y;
                }

                std::pair<double COMMA double> forward_pass(int idx) {
                    auto y_left = border_conditions(int(idx) - 1 );
                    auto y_cur = border_conditions(int(idx) );
                    auto y_right = border_conditions(int(idx) + 1 );
                    auto sigma = sigma0;
                    auto lambda_minus = (a * pow(y_cur
                    COMMA
                    sigma) +a * pow(y_left
                    COMMA
                    sigma))*0.5;
                    auto lambda_plus = (a * pow(y_cur
                    COMMA
                    sigma) +a * pow(y_right
                    COMMA
                    sigma))*0.5;
                    auto lambda_minus_prev = lambda_prev[idx];
                    if (idx - 1 < 0) {
                        lambda_minus_prev += a * pow(border_conditions(idx - 1 ), sigma);
                    } else {
                        lambda_minus_prev += lambda_prev[idx - 1];
                    }
                    lambda_minus_prev *= 0.5;
                    auto lambda_plus_prev = lambda_prev[idx];
                    if (int(idx) +1 >= x_point_number()) {
                        lambda_plus_prev += a * pow(border_conditions(int(idx) + 1 ), sigma);
                    } else {
                        lambda_plus_prev += lambda_prev[idx + 1];
                    }
                    lambda_plus_prev *= 0.5;
                    auto A = theta * dt / (dx * dx) * lambda_minus;
                    auto B = theta * dt / (dx * dx) * lambda_plus;
                    auto C = 1.0 + theta * dt / (dx * dx) * (lambda_plus + lambda_minus);
                    auto D = (lambda_plus_prev * (y_right - y_cur) - lambda_minus_prev * (y_cur - y_left)) * dt *
                             (1 - theta) / (dx * dx) + y_cur;
                    auto alpha_next = B / (C - A * alpha[idx]);
                    auto beta_next = (A * beta[idx] + D) / (C - A * alpha[idx]);
//                    std::cout<<"("<<A<<", "<<B<<", "<<C<<", "<<D<<"), ";
                    return std::pair<double COMMA double>(alpha_next COMMA beta_next);
                }

                p::tuple get_internal_state() const override {
                    auto internal_state = p::make_tuple(a, T0, sigma0, theta,
                                                         to_numpy(alpha),
                                                         to_numpy(beta),
                                                         to_numpy(lambda_prev));
                    return p::tuple(p::list(state_itf::get_internal_state()) + p::list(internal_state));
                }

                p::tuple get_init_args() const override {
                    return state_itf::get_init_args();
                }

                void set_internal_state(const p::tuple &int_state) override {
                    state_itf::set_internal_state(int_state);
                    a = p::extract<double>(int_state[default_state_len]);
                    T0 = p::extract<double>(int_state[default_state_len + 1]);
                    sigma0 = p::extract<double>(int_state[default_state_len + 2]);
                    theta = p::extract<double>(int_state[default_state_len + 3]);
                    alpha = from_numpy(p::extract<np::ndarray>(int_state[default_state_len + 4]));
                    beta = from_numpy(p::extract<np::ndarray>(int_state[default_state_len + 5]));
                    lambda_prev = from_numpy(p::extract<np::ndarray>(int_state[default_state_len + 6]));
                }

                double scheme(int i) const override {
                    auto y_cur = border_conditions(i );
                    auto y_prev = alpha[i] * y_cur + beta[i];
                    return y_prev;
                }

                double starting_conditions(int idx) const override {
                    return comp_math::default_descriptions::zero_starting_conditions(idx, this);
                }

                double border_conditions(int idx) const override {
                    if (idx < 0) {
                        auto t = it * dt;
                         
                        return T0 * pow(t, 1.0 / sigma0);
                    }
                    if (idx >= x_point_number()) {
                        return 0.0;
                    }
                    return y[idx];
                }
            })


#undef STRUCTNAME