from matplotlib import pyplot as plt
from matplotlib import animation
from data_crawler import ThreadDataCrawler as DataCrawler
import sys

if __name__ == "__main__":

    ibrae = {"explicit": ["ibrea_transfer_expl", "ibrea_transfer_expl_tuned"],
             "implicit": ["ibrae_transfer_impl", "ibrae_transfer_impl_tuned"]}
    dt = pow(10, -4)

    state = {
        "dx": 1 / 200.0,
        "dt": dt,
        "x_end": 1.0,
        "a": 1.0,
        "q": 0.1,
        # for ibrae:
        "d_x": 1.0,  # pow(10, -4),
        "d_y": 1.0,
        "wind": 10.0,
        "T0": 100,
    }
    crawl_range = 100
    draw_step = 1

    # names = ibrae["explicit"] + ibrae["implicit"]
    # names = ibrae["implicit"]
    # names = [ibrae["implicit"][1]]
    names = ["analitic_solution_transfer_lin",
             "lax_ven_linear",
             "lax_linear"]
    fig, axes = plt.subplots(nrows=3, ncols=1, figsize=(10, 7))
    # axes = [axes] * len(names)
    axes = axes.flatten()
    crawlers = []
    animations = []
    for idx, name in enumerate(names):
        crawlers.append(DataCrawler(crawl_range=crawl_range,
                                    name=name,
                                    # names=fun["linear"],
                                    init_state=state,
                                    draw_step=1,
                                    draw_profiles_only=False,
                                    limits=(-0.1, 2.0)
                                    )
                        )
        animations.append(crawlers[-1].set_plot(fig, axes[idx], pause_t=0))


    def animate(i):
        artists = []
        for anim_func in animations:
            artists += anim_func(i)
        return artists


    ani = animation.FuncAnimation(fig, animate, interval=1,
                                  blit=True, save_count=10)


    class Pause_helper:
        def __init__(self):
            self.pause = False

        def space_press(self, event):
            self.pause
            if event.key == ' ':
                self.pause ^= True
                if not self.pause:
                    # print("\rstarted")
                    ani.event_source.start()
                    return

                if self.pause:
                    # print("\rpaused")
                    ani.event_source.stop()
                    return


    pause = Pause_helper()

    fig.canvas.mpl_connect('key_press_event', pause.space_press)

    # print("crawlers are created")

    # fig.colorbar(crawlers[-1].plot_list[-1], ax=axes.ravel().tolist())
    plt.legend()
    plt.show()
