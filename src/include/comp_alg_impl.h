//
// Created by Kirill on 10/3/19.
//
/*
 * this is an .h file for state structs declarations. So this is copy-paste from a macros.
 * todo: move definitions here and implementations in corresponding cpp file
 * */

#include "interface.h"
#include "default_descriptions.h"

#ifndef COMP_MATH_COMP_ALG_IMPL_H
#define COMP_MATH_COMP_ALG_IMPL_H

#define ADD_ALG(alg_name, state_struct_name, state_struct) state_struct ;


namespace comp_math {

#include "comp_alg_descriptions.h"

}

#undef ADD_ALG


#endif //COMP_MATH_COMP_ALG_IMPL_H
