//
// Created by Kirill on 10/4/19.
//
/*
 * This is declarations for some useful functions which are expected to be wildly used.
 * For more details see corresponding cpp file
 * */

#ifndef COMP_MATH_DEFAULT_DESCRIPTIONS_H
#define COMP_MATH_DEFAULT_DESCRIPTIONS_H

#include "interface.h"

namespace comp_math {
    namespace default_descriptions {
        double periodic_border_conditions(int idx, const state_itf *state);

        border_cond_type constant_border_conditions_factory(double a, double b,
                                                            bool left_same = false, bool right_same = false);

        start_cond_type step_starting_conditions_factory(double where, double hight);

        double zero_starting_conditions(int idx, const state_itf *state);

        double half_size_border_conditions(int idx, const state_itf *state);

    };
}


#endif //COMP_MATH_DEFAULT_DESCRIPTIONS_H
