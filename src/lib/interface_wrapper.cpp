//
// Created by Kirill on 10/8/19.
//

#include "../include/interface_wrapper.h"

void comp_math::interface_wrapper::init(const p::dict &dict_state) {
    state_ptr->init(dict_state);
}

std::string comp_math::interface_wrapper::help() const {
    return state_ptr->help();
}

np::ndarray comp_math::interface_wrapper::step() {
    return state_itf::to_numpy(state_ptr->step());
}

comp_math::interface_wrapper::interface_wrapper(const comp_math::interface_wrapper &other) {
    res = other.res;
    state_ptr = other.state_ptr;
}

comp_math::interface_wrapper &comp_math::interface_wrapper::operator=(const comp_math::interface_wrapper &other) {
    return *this = other;
}

np::ndarray comp_math::interface_wrapper::result() const {
    return state_itf::to_numpy(state_ptr->y);
}

comp_math::interface_wrapper::interface_wrapper(const std::string &alg_name) {
            if (false) { ;/*just for if initializing*/}
#define ADD_ALG(name, state_struct_name, state_struct)\
            else if(alg_name == #name) {\
                state_ptr = std::move(std::unique_ptr<state_itf>(\
                    dynamic_cast<state_itf *>(new state_struct_name())\
                )); \
            }

            #include "../include/comp_alg_descriptions.h"

            else {
                throw std::runtime_error("no comp alg for such name: '" + alg_name + "'");
            }
#undef ADD_ALG
}


comp_math::interface_wrapper::interface_wrapper(comp_math::interface_wrapper &&other) noexcept {
    *this = std::move(other);
}


comp_math::interface_wrapper &comp_math::interface_wrapper::operator=(comp_math::interface_wrapper &&other) noexcept {
    state_ptr = std::move(other.state_ptr);
    return *this;
}

p::dict comp_math::interface_wrapper::get_state_dict() const {
    return state_ptr->get_state_dict();
}

p::list comp_math::interface_wrapper::iterate(int n, int skip_it) {
    int el_num = 0;
    int cur_idx = 0;
    auto& res_ref = res;
    std::function<void(np::ndarray, int cur_idx)> add_element;
    if(p::len(res_ref) < n){
        res = p::list();
        add_element = [res_ref](const np::ndarray& numpy_array, int cur_idx) mutable -> void {
            res_ref.append(numpy_array);
        };
    }else{
        add_element = [res_ref](const np::ndarray& numpy_array, int cur_idx) mutable -> void {
            res_ref[cur_idx] = numpy_array;
        };
    }
    while (cur_idx < n) {
        step();
        if (cur_idx % skip_it == 0) {
            add_element(step(), el_num);
            ++el_num;
        }
        ++cur_idx;
    }
    return p::list(res.slice(p::_, p::_));
}

p::tuple comp_math::interface_wrapper::get_internal_state() const {
    return state_ptr->get_internal_state();
}

p::tuple comp_math::interface_wrapper::get_init_args() const {
    return state_ptr->get_init_args();
}

void comp_math::interface_wrapper::set_internal_state(const p::tuple &int_state) {
    return state_ptr->set_internal_state(int_state);
}


