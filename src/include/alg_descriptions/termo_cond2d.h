//
// Created by Kirill on 12/6/19.
//

#define STRUCTNAME termocunductive_2d

    ADD_ALG(termocound_2d, STRUCTNAME,
            struct STRUCTNAME : public comp_math::state_itf {
                typedef std::vector<double> data;
                static const std::string name;
                double a = 1.0;
                double T0 = 1.0;
                double q = -1.0;
                double L0 = 1.0;
                mutable bool add_q = false;
                data Sc
                COMMA Sp
                COMMA lambda
                COMMA alpha
                COMMA beta;
                double dV;
                const double small = 1e-200;
                const double M = 1e200;
                const double Lambda_big = 1e100;
                double T0_int = 40;
                double T1_int = 50;
                double T2_int = 30;
                double T_border = 0.0;
                double q_b_int = 500.0;//500
                double a_n COMMA a_e COMMA a_s COMMA a_w COMMA a_p COMMA b;
                double ratio = 0.2;

                using comp_math::state_itf::state_itf;

                void set_ab(int i COMMA int j){
                    // std::cout<<"trying to set a's and b. idxes: "<<i<<" "<<j<<std::endl;
                    auto cur_i = i;
                    auto cur_j = j;
                    auto N = x_point_number();
                    auto adjust_idxes = [&cur_i COMMA &cur_j COMMA N]() mutable -> void{
                        if( cur_i < 0){
                            cur_i = 0;
                        }
                        if(cur_j < 0){
                            cur_j = 0;
                        }
                        if(cur_j >= N){
                            cur_j = N - 1;
                        }
                        if(cur_i >= N){
                            cur_i = N - 1;
                        }
                    };
                    cur_i = i;
                    cur_j = j - 1;
                    adjust_idxes();
                    a_n= 2*lambda.at(xy2idx(i COMMA cur_j)) * lambda.at(xy2idx(i COMMA j))/
                            (lambda.at(xy2idx(i COMMA cur_j)) + lambda.at(xy2idx(i COMMA j)) + small) / dx;
                    cur_i = i + 1;
                    cur_j = j;
                    adjust_idxes();
                    a_e= 2*lambda.at(xy2idx(cur_i COMMA j)) * lambda.at(xy2idx(i COMMA j))/
                            (lambda.at(xy2idx(cur_i COMMA j)) + lambda.at(xy2idx(i COMMA j)) + small) / dx;
                    cur_i = i;
                    cur_j = j + 1;
                    adjust_idxes();
                    a_s= 2*lambda.at(xy2idx(i COMMA cur_j)) * lambda.at(xy2idx(i COMMA j))/
                            (lambda.at(xy2idx(i COMMA cur_j)) + lambda.at(xy2idx(i COMMA j)) + small) / dx;
                    cur_i = i - 1;
                    cur_j = j;
                    adjust_idxes();
                    a_w= 2*lambda.at(xy2idx(cur_i COMMA j)) * lambda.at(xy2idx(i COMMA j))/
                            (lambda.at(xy2idx(cur_i COMMA j)) + lambda.at(xy2idx(i COMMA j)) + small) / dx;
                    a_p = a_n+ a_e+ a_s+ a_w - Sp.at(xy2idx(i COMMA j))*dV;
                    b = Sc.at(xy2idx(i COMMA j))*dV;
                    // std::cout<<"done seting a's and b"<<std::endl;
                }

                std::string get_name() const override {
                    return STRUCTNAME::name;
                }

                p::dict get_state_dict() const override {
                    auto dict = state_itf::get_state_dict();
                    dict["a"] = a;
                    dict["T0"] = T0;
                    dict["q"] = q;
                    dict["L0"] = L0;
                    dict["q_int"] = q_b_int;
                    dict["T0"] = T0_int;
                    dict["T1"] = T1_int;
                    dict["T2"] = T2_int;
                    dict["T_border"] = T_border;
                    dict["ratio"] = T2_int;
                    return dict;
                }

                std::pair<double COMMA double> hor_forward_pass(int i COMMA int j){
                    set_ab(i, j);
                    double alpha_next = a_s/ (a_p - a_n* alpha.at(j) + small);
                    double beta_next = (a_e* border_conditions(xy2idx(i + 1 COMMA j)) +
                                    a_w* border_conditions(xy2idx(i - 1 COMMA j)) + b +
                                    a_n * beta.at(j))/(a_p - a_n* alpha.at(j) + small);
                    return std::pair<double COMMA double>(alpha_next COMMA beta_next);//returning alpha[x + 1], beta[x + 1]
                }

                std::pair<double COMMA double> vert_forward_pass(int i COMMA int j){
                    set_ab(i, j);
                    // printf("(%d, %d): a's: %f, %f, %f, %f, %f\n",i, j, a_p, a_w, a_e, a_n, a_s);
                    double alpha_next = a_e/ (a_p - a_w* alpha.at(i) + small);
                    double beta_next = (a_n* border_conditions(xy2idx(i COMMA j - 1)) +
                                    a_s* border_conditions(xy2idx(i COMMA j + 1)) + b +
                                    a_w* beta.at(i)) / (a_p - a_w* alpha.at(i) + small);
                    return std::pair<double COMMA double>(alpha_next COMMA beta_next);//returning alpha[x + 1], beta[x + 1]
                }

                void init(const p::dict &dict_state) override {
                    state_itf::init(dict_state);
                    a = p::extract<double>(dict_state["a"]);
                    T0 = p::extract<double>(dict_state["T0"]);
                    q = p::extract<double>(dict_state["q"]);
                    L0 = p::extract<double>(dict_state["L0"]);
                    q_b_int = p::extract<double>(dict_state["q_int"]);
                    T0_int = p::extract<double>(dict_state["T0"]);
                    T1_int = p::extract<double>(dict_state["T1"]);
                    T2_int = p::extract<double>(dict_state["T2"]);
                    T_border = p::extract<double>(dict_state["T_border"]);
                    ratio = p::extract<double>(dict_state["ratio"]);

                    y = std::vector<double>(x_point_number() * x_point_number());
                    beta = std::vector<double>(x_point_number() + 1 COMMA 0.0);
                    alpha = std::vector<double>(x_point_number() + 1 COMMA 0.0);
                    dV = dx * dx;
                    Sc = data(y.size() COMMA 0.0);
                    Sp = data(y.size() COMMA 0.0);
                    lambda = data(y.size() COMMA L0);
                    double s_xc = x_point_number()/2.0;
                    double s_yc = x_point_number()/5.0;
                    const double N = x_point_number();
                    auto border_lower_left_right = [ratio=this->ratio COMMA N](int i COMMA int j) -> bool{
                        return i > N*ratio - 2 and i < N*ratio + 2 and j > (1 - ratio)*N + 1;
                    };
                    auto border_lower_left_upper = [ratio=this->ratio COMMA N](int i COMMA int j) -> bool{
                        return j > N*(1 - ratio) - 2 and j < N*(1 - ratio) + 2 and i < ratio*N - 2;
                    };
                    auto border_upper_right_left = [ratio=this->ratio COMMA N](int i COMMA int j) -> bool{
                        return i > N*(1 - ratio) - 1 and i < N*(1 - ratio) + 1 and j < ratio*N - 2;
                    };
                    
                    auto border_lower = [ratio=this->ratio COMMA N](int i COMMA int j) -> bool{
                        return j == N - 1;
                    };
                    auto border_right = [ratio=this->ratio COMMA N](int i COMMA int j) -> bool{
                        return i == N - 1;
                    };
                    auto border_left = [ratio=this->ratio COMMA N](int i COMMA int j) -> bool{
                        return i == 0;
                    };
                    auto border_upper = [ratio=this->ratio COMMA N](int i COMMA int j) -> bool{
                        return j == 0;
                    };
                    auto central_source = [ratio=this->ratio COMMA N](int s_xc, int s_yc, int x COMMA int y) -> bool{
                        return (x - s_xc)*(x - s_xc) + (y - s_yc)*(y - s_yc) < N*ratio*N*ratio*ratio/4.0;
                    };
                    auto upper_left = [ratio=this->ratio COMMA N](int i COMMA int j) -> bool {
                        return i < int(ratio*N) and j < int(ratio*N);
                    };
                    auto lower_left = [ratio=this->ratio COMMA N](int i COMMA int j) -> bool {
                        return i < int(ratio*N) and j > int((1 - ratio)*N);
                    };
                    auto upper_right = [ratio=this->ratio COMMA N](int i COMMA int j) -> bool {
                        return i > int((1 - ratio)*N) and j < int(ratio*N);
                    };
                    auto lower_right = [ratio=this->ratio COMMA N](int i COMMA int j) -> bool {
                        return i > int((1 - ratio)*N) and j > int((1 - ratio)*N);
                    };
                    auto general_border = [N](int i COMMA int j) -> bool{
                        return i == 0 or j == 0 or i == N - 1 or j == N -1;
                    };
                    auto left_half = [N](int i COMMA int j) -> bool{
                        return i <= N/2;
                    };
                    for(int i = 0; i < x_point_number(); ++i) {//grate cycle of starting conditions
                        for(int j = 0; j < x_point_number(); ++j){
                            const int cur_idx = xy2idx(i COMMA j);
                            if(upper_left(i COMMA j)){//check
                                    lambda.at(cur_idx) = Lambda_big;//upper left
                            }
                            if(lower_left(i COMMA j)){//check
                                    lambda.at(cur_idx) = small;//lower left
                            }
                            if(upper_right(i COMMA j)){
                                lambda.at(cur_idx) = small;//upper right
                            }
                            if(lower_right(i COMMA j)){//check
                                lambda.at(cur_idx) = small;//lower right
                            }
                            if(central_source(N/2 COMMA N/2 COMMA i COMMA j)){//circle in the middle
                                Sc.at(cur_idx) = M*T0;
                                Sp.at(cur_idx) = -M;
                                lambda.at(cur_idx) = Lambda_big;
                            }

                            // if(central_source(N/2 COMMA 3*N/4 COMMA i COMMA j)){//lower circle in the middle
                            //     Sp.at(cur_idx) = -M*T0;
                            //     Sc.at(cur_idx) = M;
                            //     lambda.at(cur_idx) = Lambda_big;
                            // }

                            if(border_lower_left_right(i COMMA j)){
                                Sc.at(cur_idx) = dx/dV * 2*L0/dx * T2_int;
                                Sp.at(cur_idx) = -dx/dV * 2*L0/dx;
                            }
                            // if(border_upper_right_left(i COMMA j)){
                            //     lambda.at(cur_idx) = small;
                            // }
                            if(border_lower_left_upper(i COMMA j)){
                                Sc.at(cur_idx) = dx/dV * 2*L0/dx * T1_int;
                                Sp.at(cur_idx) = -dx/dV * 2*L0/dx;
                            }                        
                            // if(border_right(i COMMA j)){
                            //     Sc.at(cur_idx) = M * T0_int;
                            //     Sp.at(cur_idx) = -M;
                            // }
                            if(border_left(i COMMA j)){
                                ;
                            }
                            if(border_upper(i COMMA j)){
                                ;//absolute zero as in border conditions
                            }
                            if(border_lower(i COMMA j)){
                                ;//this one occupied by free end. It is defined in border conditions bc it not a static source
                            }
                        }
                    }
                    // std::cout<<"lambda:";
                    // for(int i = 0; i < y.size(); ++i){
                    //     if(i % x_point_number() == 0){
                    //         std::cout<<std::endl;
                    //     }
                    //     std::cout<<lambda.at(i)<<" ";
                    // }
                    // std::cout<<std::endl;
                
                    // std::cout<<"border_conditions:";
                    // for(int i = -1; i < N + 1; ++i){
                    //     std::cout<<std::endl;
                    //     for(int j = -1; j < N + 1; ++j){
                    //         std::cout<<border_conditions(xy2idx(i , j))<<" ";
                    //     }
                    // }

                    // std::cout<<std::endl<<"end of border_conditions"<<std::endl;
                }

                std::string help() override {
                    return state_itf::help() + "a: " +
                           "T0: " + std::to_string(T0_int) + "\n" + 
                           "T1: " + std::to_string(T1_int) + "\n" + 
                           "T2: " + std::to_string(T2_int) + "\n" + 
                           "L0: " + std::to_string(L0) + "\n" + 
                           "ratio: " + std::to_string(ratio) + "\n" + 
                           "T_border: " + std::to_string(T_border) + "\n" + 
                            "";

                }

                std::pair<int COMMA int> idx2xy(int idx) const {
                    int y = idx / x_point_number();
                    int x = idx - y*x_point_number();
                    return std::pair<int COMMA int>(x COMMA y);
                }

                int xy2idx(int x COMMA int y) const {
                    int bound = 0;
                    if (x < bound) {
//                        return y*x_point_number();
                        return -1;
                    }
                    if (y < bound) {
                        return -2;
                    }
                    if (x >= x_point_number() - bound) {
//                        add_q = true;
//                        return x_point_number() - 1 + y*x_point_number();
                        return -1;
                    }
                    if (y >= x_point_number() - bound) {
                        return -4;
                    }
                    return x + y * x_point_number();
                }

                std::vector<double> step() override {
                    std::vector<double> new_y = y;
                    const int N = x_point_number();
                    // std::cout<<"entering horisontal session"<<std::endl;
                    for(int i = 0; i < x_point_number(); ++i) {
                        // std::cout<<"assigning betas"<<std::endl;
                        beta.at(0) = border_conditions(xy2idx(i, -1));
                        beta.at(N) = border_conditions(xy2idx(N, 0));
                        // std::cout<<"alph & beta cycle"<<std::endl;
                        for(int j = 0; j < x_point_number() - 1; ++j){
                            auto tmp_pair = hor_forward_pass(i COMMA j);
                            // std::cout<<"idx: "<<i<<" "<<j<<std::endl;
                            alpha.at(j + 1) = tmp_pair.first;
                            beta.at(j + 1) = tmp_pair.second;
                        }
                        // std::cout<<"step cycle"<<std::endl;
                        for (int j = x_point_number() - 1; j > -1; --j) {
                            new_y.at(xy2idx(i COMMA j)) = alpha.at(j + 1)*border_conditions(xy2idx(i, j + 1)) + beta.at(j + 1);
                        }
                    }
                    // std::cout<<"exited horisontal session"<<std::endl;

                    // std::cout<<"y_{n + 0.5}:";
                    // for(size_t i = 0; i < y.size(); ++i){
                    //     if(i % x_point_number() == 0){
                    //         std::cout<<std::endl;
                    //     }
                    //     std::cout<<y.at(i)<<" ";
                    // }
                    // std::cout<<std::endl;

                    y = new_y;

                    // std::cout<<"entering vertical session"<<std::endl;
                    for(int j = 0; j < x_point_number(); ++j) {
                        beta.at(0) = border_conditions(xy2idx(-1, j));
                        beta.at(N) = border_conditions(xy2idx(N, 0));
                        for(int i = 0; i < x_point_number() - 1; ++i){
                            auto tmp_pair = vert_forward_pass(i COMMA j);
                            // std::cout<<"("<<tmp_pair.first<<"."<<tmp_pair.second<<")"<<std::endl;
                            alpha.at(i + 1) = tmp_pair.first;
                            beta.at(i + 1) = tmp_pair.second;
                        }
                        for (int i = x_point_number() - 1; i > -1; --i) {
                            new_y.at(xy2idx(i COMMA j)) = alpha.at(i + 1)*border_conditions(xy2idx(i + 1, j)) + beta.at(i + 1);
                        }
                    }
                    // std::cout<<"exited vertical session"<<std::endl;



                    
                    y = std::move(new_y);
                    //y = lambda;//for debug
                    ++it;

                    // std::cout<<"y_{n + 1}:";
                    // for(size_t i = 0; i < y.size(); ++i){
                    //     if(i % x_point_number() == 0){
                    //         std::cout<<std::endl;
                    //     }
                    //     std::cout<<y.at(i)<<" ";
                    // }
                    // std::cout<<std::endl;
                    return y;
                }
            },
            [](int i COMMA const comp_math::state_itf *state) -> double {
                return -1.0; //never called
            },
            [](int idx COMMA const comp_math::state_itf *state) -> double {
                auto spec_state = dynamic_cast<const comp_math::STRUCTNAME *>(state);
                double add = 0;
//                if(idx == -2){
//                    return spec_state->T0;
//                }
//                if(idx == -4){
//                    return spec_state->T0/10.0;
//                }
//                if(spec_state->add_q){
//                    spec_state->add_q = false;
//                    add = spec_state->q/spec_state->dx*spec_state->lambda[idx];
//                }
                //fixed q is made with the help of xy2idx function and "double add"
                if(idx < 0){
                    return 0.0;
                }
                const int N = spec_state->x_point_number();
                int x;
                int y;
                auto idx_pair = spec_state->idx2xy(idx);
                x = idx_pair.first;
                y = idx_pair.second;
                if(y == 0 and x <= spec_state->ratio*N){//upper left cell's upper border
                    return 100;
                }
                if(x == 0 and y <= spec_state->ratio*N){//upper left cell's upper border
                    return 100;
                }
                if(y == int((1 - spec_state->ratio)*N) and x >= (1 - spec_state->ratio)*N){//lower right cell's upper border
                    return spec_state->y.at(spec_state->xy2idx(x, y - 1));
                }
                if(y >= (1 - spec_state->ratio)*N and x == int((1 - spec_state->ratio)*N)){//lower right cell's left border
                    return spec_state->y.at(spec_state->xy2idx(x - 1, y));
                }
                if(x == int((1 - spec_state->ratio)*N) - 1 and y <= spec_state->ratio*N){//upper right cell's left border q != 0
                    return state->y.at(spec_state->xy2idx(x - 1 COMMA y)) + spec_state->q_b_int;//it is ok bc y is N >> 2
                }

                if(y == N - 1){//lower border q = 0
                    return state->y.at(spec_state->xy2idx(x COMMA y - 1));//it is ok bc y is N >> 2
                }
                if(x == N - 1 and y >= spec_state->ratio*N - 1 and  y <= (1 - spec_state->ratio)*N){//right border q != 0
                    return state->y.at(spec_state->xy2idx(x - 1 COMMA y)) + spec_state->q_b_int;//it is ok bc y is N >> 2
                }
                if(x == 0 and y > spec_state->ratio*N and y < (1 - spec_state->ratio)*N){//left border T = T1_int
                    return spec_state->T_border;//it is ok bc y is N >> 2
                }
                if(y == 0){//upper border - zero
                    return 0.0;
                }
                return state->y.at(idx);
            },
            comp_math::default_descriptions::zero_starting_conditions)

#undef STRUCTNAME

