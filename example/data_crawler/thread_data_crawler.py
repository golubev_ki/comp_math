from comp_math import Comp_engine
import multiprocessing as mp
import numpy as np
from time import sleep
from time import time


class ThreadDataCrawler:

    def __init__(self, crawl_range, name, init_state, draw_profiles_only=False, draw_step=1, limits=(0.0,1.0)):
        self.name = name
        self.active_storage = []
        self.passive_storage = []
        self.engine = Comp_engine(name)
        self.engine.init(init_state)
        self.point_num = self.engine.get_state()["N"]
        self.buffer_size = self.engine.get_state()["real_N"]
        self.crawl_range = crawl_range
        self.plot = None
        self.ax = None
        self.draw_step = draw_step
        self.heatmap = self.buffer_size == self.point_num * self.point_num
        self.draw_profiles_only = draw_profiles_only
        if draw_profiles_only:
            if not self.heatmap:
                raise ValueError("You can't draw profiles if there is no 2D data")
            self.heatmap = False
        self.limits = limits
        self.parent_pipe_end, self.child_pipe_end = mp.Pipe()
        self.data_thread = mp.Process(target=self.run_for_data, args=(self.engine,
                                                                        self.crawl_range,
                                                                        self.draw_step,
                                                                        self.child_pipe_end,)
        )
        self.data_thread.start()

    def reset_thread_storage(self):
        self.active_storage = []

    @staticmethod
    def run_for_data(engine, crawl_range, draw_step, pipe):
        data = engine.iterate(crawl_range, draw_step)
        pipe.send([data, engine])  # Pickling of "comp_math.comp_math.Comp_engine"

    def data_to_plot(self):
        if len(self.active_storage) == 0:
            start = time()
            self.passive_storage, self.engine = self.parent_pipe_end.recv()
            thread_waiting_start = time()
            #print("waiting for join:", end = '')
            self.data_thread.join()
            #print(time() - thread_waiting_start)
            #print("data for " + self.name + " is harvested in {:.3f}sec".format(time() - start))
            # ------------
            proc_start = time()
            #print("creating proc...", end='')
            self.data_thread = mp.Process(target=self.run_for_data, args=(self.engine,
                                                                        self.crawl_range,
                                                                        self.draw_step,
                                                                        self.child_pipe_end,)
                                            )
            self.active_storage = self.passive_storage
            self.passive_storage = None
            #print("\rcreated:", time() - proc_start)
            # -------------
            #print("harvesting data...")
            self.data_thread.start()
            # start routine for passive storage

        plot_data = self.active_storage[0]
        del self.active_storage[0]
        return plot_data

    def set_plot(self, fig, ax, pause_t=0):
        
        ax.set_title(self.name)

        if not self.heatmap:
            ax.set_xlim([0, self.point_num])
            ax.set_ylim([self.limits[0], self.limits[1]])

        if self.heatmap:
            self.plot = ax.imshow(
                np.zeros(shape=(self.point_num, self.point_num)),
                vmax=self.limits[1], vmin=self.limits[0], cmap='hot')
        else:
            self.plot = ax.plot([np.nan] * self.point_num, label=self.name)[0]

        def animate(i):
            if pause_t != 0:
                sleep(pause_t)
            cur_state = self.engine.get_state()


            plot_data = self.data_to_plot()


            if self.heatmap:
                self.plot.set_data(
                    plot_data.reshape((self.point_num, self.point_num))
                )
            else:
                if self.draw_profiles_only:
                    data = plot_data.reshape((self.point_num, self.point_num))[self.point_num // 4, :]
                    self.plot.set_ydata(data)
                else:
                    self.plot.set_ydata(plot_data)
                    # print(self.plot)
                self.plot.set_label(self.name)
            return self.plot,

        return animate

    def __iter__(self):
        return self

    def __next__(self):
        return self.data_to_plot()

    def __del__(self):
        self.child_pipe_end.close()
        self.parent_pipe_end.close()
        pass
