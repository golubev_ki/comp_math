//
// Created by Kirill on 10/8/19.
//

#include "../include/interface.h"

comp_math::state_itf::state_itf() {
    //cout<<"entering state interface constructor"<<endl;
    dx = 0.1;
    dt = 0.1;
    x_end = 1.0;
    it = 0;
    y = std::vector<double>(x_point_number());
    default_state_len = p::len(get_internal_state());
}


std::vector<double> comp_math::state_itf::step() {
    auto new_y = std::vector<double>(y.size());
    for (int i = 0; i < y.size(); ++i) {
        new_y[i] = scheme(i);
    }
    y = std::move(new_y);
    ++it;
    return y;
}

void comp_math::state_itf::init(const p::dict &dict_state) {
    x_end = p::extract<double>(dict_state["x_end"]);
    dx = p::extract<double>(dict_state["dx"]);
    dt = p::extract<double>(dict_state["dt"]);
    y = std::vector<double>(x_point_number());
    set_starting_conditions();
}

std::string comp_math::state_itf::help() {
    return "name: " + get_name() + "\n" +
           "it: " + std::to_string(it) + "\n" +
           "x_end: " + std::to_string(x_end) + "\n" +
           "dx: " + std::to_string(dx) + "\n" +
           "dt: " + std::to_string(dt) + "\n" +
           "N: " + std::to_string(x_point_number()) + "\n";
}

p::dict comp_math::state_itf::get_state_dict() const {
    auto dict_state = p::dict();
    dict_state["y"] = to_numpy(this->y);
    dict_state["it"] = this->it;
    dict_state["dt"] = this->dt;
    dict_state["dx"] = this->dx;
    dict_state["x_end"] = this->x_end;
    dict_state["N"] = this->x_point_number();
    dict_state["real_N"] = this->y.size();
    return dict_state;
}

int comp_math::state_itf::x_point_number() const {
    return int(x_end / dx) + 1;
}

void comp_math::state_itf::set_starting_conditions() {
    for (int i = 0; i < y.size(); ++i) {
        y[i] = this->starting_conditions(i);
    }
}

std::vector<double> comp_math::state_itf::from_numpy(const np::ndarray &arr) {
    auto len = arr.shape(0);
    auto input_ptr = reinterpret_cast<double *>(arr.get_data());
    std::vector<double> res(len);
    std::copy(input_ptr, input_ptr + len, res.data());
    return res;
}

np::ndarray comp_math::state_itf::to_numpy(const std::vector<double> &vec) {
    auto copy = new double[vec.size()];
    std::copy(vec.data(), vec.data() + vec.size(), copy);
    return np::from_data(copy, np::dtype::get_builtin<double>(),
                         p::make_tuple(vec.size()),
                         p::make_tuple(sizeof(double)),
                         p::object());
}

p::tuple comp_math::state_itf::get_init_args() const {
    return p::make_tuple(this->get_name());
}

p::tuple comp_math::state_itf::get_internal_state() const {
    auto state = p::make_tuple(
                to_numpy(y), it, dx, dt, x_end
            );
    return state;
}

void comp_math::state_itf::set_internal_state(const p::tuple &int_state) {
    y = from_numpy(p::extract<np::ndarray>(int_state[0]));
    it = p::extract<int>(int_state[1]);
    dx = p::extract<double>(int_state[2]);
    dt = p::extract<double>(int_state[3]);
    x_end = p::extract<double>(int_state[4]);
}


p::list comp_math::state_itf::to_list(const std::vector<double> &vec) {
    p::object get_iter = p::iterator < std::vector < double > > ();
    p::object iter = get_iter(vec);
    p::list l(iter);
    return l;
}

std::vector<double> comp_math::state_itf::to_vector(const p::list &l) {
    auto res = std::vector<double>(p::len(l));
    for (int i = 0; i < res.size(); ++i) {
        res[i] = p::extract<double>(l[i]);
    }
    return res;
}
