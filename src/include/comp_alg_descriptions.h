//
// Created by Kirill on 10/3/19.
//

/*This is a configureation file with a macroses which are to define the computational algorith to use
 * first macros signature: ADD_ALG(name, struture name,
 *                                  struct description)
 * - name - is the name which will be assign to this computational engine and is used to create one when
 * calling it in throught python
 * - structure name - struct name which is parametrised through STRUCTNAME define
 * to avoid name mismatch errors during extending.
 * */


#include "alg_descriptions/termocund_nonlin.h"
#include "alg_descriptions/termocund_lin.h"
#include "alg_descriptions/transfer_nonlin.h"
#include "alg_descriptions/transfer_lin.h"
// #include "alg_descriptions/earth_cooling.h"
#include "alg_descriptions/ibrae_diffusion2d_expl.h"
#include "alg_descriptions/ibrae_diffusion2d_impl.h"
//#include "alg_descriptions/termo_cond2d.h"



