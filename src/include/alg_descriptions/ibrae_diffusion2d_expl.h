//
// Created by Kirill on 11/30/19.
//

#define STRUCTNAME ibrae_transfer_expl

ADD_ALG(ibrea_transfer_expl, STRUCTNAME,
        struct STRUCTNAME : public comp_math::state_itf {
            static const std::string name;
            double d_x COMMA d_y COMMA wind;
            using comp_math::state_itf::state_itf;


            std::string get_name() const override {
                return STRUCTNAME::name;
            }

            p::dict get_state_dict() const override {
                auto dict = state_itf::get_state_dict();
                dict["d_x"] = d_x;
                dict["d_y"] = d_y;
                dict["wind"] = wind;
                return dict;
            }

            std::pair<int COMMA int> idx_2_x_y(int idx) const {
                return std::pair<int COMMA int>(idx % x_point_number() COMMA idx / x_point_number());
            }

            int x_y_2_idx(int x COMMA int y) const {
                if(x < 0 or x >= x_point_number()){
                    return -1;
                }
                if(y < 0 or y >= x_point_number()){
                    return -1;
                }
                return x + y * x_point_number();
            }

            void init(const p::dict &dict_state) override {

                state_itf::init(dict_state);
                d_x = p::extract<double>(dict_state["d_x"]);
                d_y = p::extract<double>(dict_state["d_y"]);
                wind = p::extract<double>(dict_state["wind"]);

                y = std::vector<double>(x_point_number() * x_point_number() COMMA 0);
                set_starting_conditions();
            }

            std::string help() override {
                return state_itf::help() + "\n" +
                "d_x: " + std::to_string(d_x) + "\n" +
                "d_y: " + std::to_string(d_y) + "\n" +
                "wind: " + std::to_string(wind) + "\n";
            }

            p::tuple get_internal_state() const override {
                auto internal_state = p::make_tuple(d_x, d_y, wind);
                return p::tuple(p::list(state_itf::get_internal_state()) + p::list(internal_state));
            }

            p::tuple get_init_args() const override {
                return state_itf::get_init_args();
            }

            void set_internal_state(const p::tuple &int_state) override {
                state_itf::set_internal_state(int_state);
                d_x = p::extract<double>(int_state[default_state_len]);
                d_y = p::extract<double>(int_state[default_state_len + 1]);
                wind = p::extract<double>(int_state[default_state_len + 2]);
            }

            double scheme(int idx) const override {
                    auto cur_dx = idx;
                    auto tmp_idx = idx_2_x_y(idx);
                    auto hx = dx;
                    auto hy = dx;//intentional dy = dx
                    int i = tmp_idx.first;
                    int j = tmp_idx.second;
                    auto C_cur =  border_conditions( x_y_2_idx(i COMMA j));
                    auto C_w =  border_conditions( x_y_2_idx(i - 1 COMMA j));
                    auto C_e =  border_conditions( x_y_2_idx(i + 1 COMMA j));
                    auto C_n =  border_conditions( x_y_2_idx(i COMMA j - 1));
                    auto C_s =  border_conditions( x_y_2_idx(i COMMA j + 1));
                    auto D_x = dt* d_x/(hx*hx);
                    auto D_y = dt* d_y/(hy*hy);
                    //auto C_next = C_cur + D_x*(C_e - 2*C_cur + C_w) + D_y*(C_s - 2*C_cur + C_n);
                    auto C_next = C_cur -  wind*dt/(2*hx)*(C_e - C_w) + D_x*(C_e - 2*C_cur + C_w) + D_y*(C_s - 2*C_cur + C_n);//f is out
                    return C_next;
                }

                double starting_conditions(int idx) const override {
                    return comp_math::default_descriptions::zero_starting_conditions(idx, this);
                }

                double border_conditions(int idx) const override {
                    return comp_math::default_descriptions::half_size_border_conditions(idx, this);
                }

        })

#undef STRUCTNAME

#define STRUCTNAME ibrae_transfer_expl_tuned

    ADD_ALG(ibrea_transfer_expl_tuned, STRUCTNAME,
            struct STRUCTNAME : public comp_math::state_itf {
                static const std::string name;
                double d_x COMMA d_y COMMA wind;
                using comp_math::state_itf::state_itf;


                std::string get_name() const override {
                    return STRUCTNAME::name;
                }

                p::dict get_state_dict() const override {
                    auto dict = state_itf::get_state_dict();
                    dict["d_x"] = d_x;
                    dict["d_y"] = d_y;
                    dict["wind"] = wind;
                    return dict;
                }

                std::pair<int COMMA int> idx_2_x_y(int idx) const {
                    return std::pair<int COMMA int>(idx % x_point_number() COMMA idx / x_point_number());
                }

                int x_y_2_idx(int x COMMA int y) const {
                    if(x < 0 or x >= x_point_number()){
                        return -1;
                    }
                    if(y < 0 or y >= x_point_number()){
                        return -1;
                    }
                    return x + y * x_point_number();
                }

                std::vector<double> step() override {
                    return state_itf::step();
                }

                void init(const p::dict &dict_state) override {

                    state_itf::init(dict_state);
                    d_x = p::extract<double>(dict_state["d_x"]);
                    d_y = p::extract<double>(dict_state["d_y"]);
                    wind = p::extract<double>(dict_state["wind"]);

                    y = std::vector<double>(x_point_number() * x_point_number() COMMA 0);
                    set_starting_conditions();
                }

                p::tuple get_internal_state() const override {
                    auto internal_state = p::make_tuple(d_x, d_y, wind);
                    return p::tuple(p::list(state_itf::get_internal_state()) + p::list(internal_state));
                }

                p::tuple get_init_args() const override {
                    return state_itf::get_init_args();
                }

                void set_internal_state(const p::tuple &int_state) override {
                    state_itf::set_internal_state(int_state);
                    d_x = p::extract<double>(int_state[default_state_len]);
                    d_y = p::extract<double>(int_state[default_state_len + 1]);
                    wind = p::extract<double>(int_state[default_state_len + 2]);
                }

                std::string help() override {
                    return state_itf::help() + "\n" +
                           "d_x: " + std::to_string(d_x) + "\n" +
                           "d_y: " + std::to_string(d_y) + "\n" +
                           "wind: " + std::to_string(wind) + "\n";
                }

                double scheme(int idx) const override {
                    auto cur_dx = idx;
                    auto tmp_idx =  idx_2_x_y(idx);
                    auto hx =  dx;
                    auto hy =  dx;//intentional dy = dx
                    int i = tmp_idx.first COMMA j = tmp_idx.second;
                    auto C_cur =  border_conditions( x_y_2_idx(i COMMA j));
                    auto C_w =  border_conditions( x_y_2_idx(i - 1 COMMA j));
                    auto C_e =  border_conditions( x_y_2_idx(i + 1 COMMA j));
                    auto C_n =  border_conditions( x_y_2_idx(i COMMA j - 1));
                    auto C_s =  border_conditions( x_y_2_idx(i COMMA j + 1));
                    auto D_x = dt* d_x/(hx*hx);
                    auto D_y = dt* d_y/(hy*hy);
                    //auto C_next = C_cur + D_x*(C_e - 2*C_cur + C_w) + D_y*(C_s - 2*C_cur + C_n);
                    auto C_next = C_cur -  wind*dt/hx*(C_cur - C_w) + D_x*(C_e - 2*C_cur + C_w) + D_y*(C_s - 2*C_cur + C_n);//f is out
                    return C_next;
                }

                double starting_conditions(int idx) const override {
                    return comp_math::default_descriptions::zero_starting_conditions(idx, this);
                }

                double border_conditions(int idx) const override {
                    return comp_math::default_descriptions::half_size_border_conditions(idx, this);
                }
            }
    )

#undef STRUCTNAME

