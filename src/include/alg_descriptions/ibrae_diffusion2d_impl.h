//
// Created by Kirill on 12/1/19.
//

#define STRUCTNAME ibrae_transfer_impl

    ADD_ALG(ibrae_transfer_impl, STRUCTNAME,
            struct STRUCTNAME : public comp_math::state_itf {
                static const std::string name;
                double d_x;
                double d_y;
                double wind;
                double p;
                double w;
                double s;
                double e;
                double n;

                Eigen::SparseMatrix<double> impl_scheme;
                Eigen::BiCGSTAB<Eigen::SparseMatrix<double>> solver;

                p::tuple get_internal_state() const override {
                    auto internal_state = p::make_tuple(d_x, d_y, wind, p, w, s, e, n);
                    return p::tuple(p::list(state_itf::get_internal_state()) + p::list(internal_state));
                }

                p::tuple get_init_args() const override {
                    return state_itf::get_init_args();
                }

                void set_internal_state(const p::tuple &int_state) override {
                    state_itf::set_internal_state(int_state);
                    d_x = p::extract<double>(int_state[default_state_len]);
                    d_y = p::extract<double>(int_state[default_state_len + 1]);
                    wind = p::extract<double>(int_state[default_state_len + 2]);
                    p = p::extract<double>(int_state[default_state_len + 3]);
                    w = p::extract<double>(int_state[default_state_len + 4]);
                    s = p::extract<double>(int_state[default_state_len + 5]);
                    e = p::extract<double>(int_state[default_state_len + 6]);
                    n = p::extract<double>(int_state[default_state_len + 7]);
                    impl_scheme = init_impl_scheme_matrix();
                    solver.compute(impl_scheme);
                    if (solver.info() != Eigen::Success) {
                        std::cerr << "decomposition failed. Likely you will have some rubbish results" << std::endl;
                    }
                }

                using comp_math::state_itf::state_itf;

                std::string get_name() const override {
                    return STRUCTNAME::name;
                }

                p::dict get_state_dict() const override {
                    auto dict = state_itf::get_state_dict();
                    dict["d_x"] = d_x;
                    dict["d_y"] = d_y;
                    dict["wind"] = wind;
                    return dict;
                }

                std::pair<int COMMA int> idx2xy(int idx) const {
                    return std::pair<int COMMA int>(idx % x_point_number() COMMA idx / x_point_number());
                }

                int xy2idx(int x COMMA int y) const {
                    if(x < 0 or x >= x_point_number()){
                        return -1;
                    }
                    if(y < 0 or y >= x_point_number()){
                        return -1;
                    }
                    return x + y * x_point_number();
                }

                std::vector<double> step() override {
                    auto b = get_impl_scheme_vector();

                    Eigen::VectorXd C = solver.solve(b);
                    if (solver.info() != Eigen::Success) {
                        std::cerr << "solver failed. Likely you will have some rubbish instead of results" << std::endl;
                    }
                    y = std::vector<double>(C.data(), C.data() + y.size());
                    ++it;
                    return y;
                }

                void init(const p::dict &dict_state) override {
                    state_itf::init(dict_state);
                    d_x = p::extract<double>(dict_state["d_x"]);
                    d_y = p::extract<double>(dict_state["d_y"]);
                    wind = p::extract<double>(dict_state["wind"]);

                    y = std::vector<double>(x_point_number() * x_point_number() COMMA 0);
                    set_starting_conditions();

                    //these are coefs near C_{i,j} C{i-1, j}, C{i+1, j} and so on. hint w: west, e: east
                    //matrix: (x_point_number()^2, x_point_number()^2)

                    p = 1.0 + 2.0 * d_x * dt / (dx * dx) + 2.0 * d_y * dt / (dx * dx);
                    w = - d_x * dt / (dx * dx) + dt / (2.0*dx) * wind;
                    e = - d_x * dt / (dx * dx) - dt / (2.0*dx) * wind;
                    n = - d_y * dt / (dx * dx);
                    s = - d_y * dt / (dx * dx);

                    impl_scheme = init_impl_scheme_matrix();
                    solver.compute(impl_scheme);
                    if (solver.info() != Eigen::Success) {
                        std::cerr << "decomposition failed. Likely you will have some rubbish results" << std::endl;
                    }
                }

                Eigen::SparseMatrix<double COMMA Eigen::RowMajor>
                init_impl_scheme_matrix() const{
                    //these are coefs near C_{i,j} C{i-1, j}, C{i+1, j} and so on. hint w: west, e: east
                    //matrix: (x_point_number()^2, x_point_number()^2)
                    const auto N = y.size();
                    auto impl_scheme_t = Eigen::SparseMatrix<double COMMA Eigen::RowMajor>(N COMMA N);

                    impl_scheme_t.reserve(Eigen::VectorXi::Constant(N COMMA 5));

                    std::cout << std::fixed << std::setprecision(3);
                    for (int idx = 0; idx < N; ++idx) {
                        const int i = idx;
                        auto idx_pair = idx2xy(idx);
                        int x = idx_pair.first;
                        int y = idx_pair.second;
                        if (int new_idx = xy2idx(x COMMA y - 1); new_idx >= 0) {
                            impl_scheme_t.insert(new_idx COMMA i) = n;
                        }
                        if (int new_idx = xy2idx(x - 1 COMMA y); new_idx >= 0) {
                            impl_scheme_t.insert(new_idx COMMA i) = w;
                        }

                        impl_scheme_t.insert(i COMMA i) = p;

                        if (int new_idx = xy2idx(x + 1 COMMA y); new_idx >= 0) {
                            impl_scheme_t.insert(new_idx COMMA i) = e;
                        }
                        if (int new_idx = xy2idx(x COMMA y + 1); new_idx >= 0) {
                            impl_scheme_t.insert(new_idx COMMA i) = s;
                        }
                    }
                    impl_scheme_t.makeCompressed();

                    return impl_scheme_t;
                }

                Eigen::VectorXd get_impl_scheme_vector() const {
                    //these are coefs near C_{i,j} C{i-1, j}, C{i+1, j} and so on. hint w: west, e: east
                    //matrix: (x_point_number()^2, x_point_number()^2)

                    auto result = Eigen::VectorXd(y.size());
                    for (int i = 0; i < y.size(); ++i) {
                        auto idx_pair = idx2xy(i);
                        auto x = idx_pair.first;
                        auto y = idx_pair.second;
                        result(i) = border_conditions(xy2idx(x COMMA y));

                        if(int new_idx = xy2idx(x COMMA y - 1); new_idx < 0){
                            result(i) -= n * border_conditions(new_idx);

                        }
                        if(int new_idx = xy2idx(x - 1 COMMA y); new_idx < 0){
                            result(i) -= w * border_conditions(new_idx);

                        }
                        if(int new_idx = xy2idx(x + 1 COMMA y); new_idx < 0){
                            result(i) -= e * border_conditions(new_idx);

                        }
                        if(int new_idx = xy2idx(x COMMA y + 1); new_idx < 0){
                            result(i) -= s * border_conditions(new_idx);

                        }

                    }

                    return result;

                }

                std::string help() override {
                    return state_itf::help() + "\n" +
                           "d_x: " + std::to_string(d_x) + "\n" +
                           "d_y: " + std::to_string(d_y) + "\n" +
                           "wind: " + std::to_string(wind) + "\n";
                }
                double scheme(int i) const override {
                    return - 1; //it's never used
                }

                double starting_conditions(int idx) const override {
                    return comp_math::default_descriptions::zero_starting_conditions(idx, this);
                }

                double border_conditions(int idx) const override {
                    return comp_math::default_descriptions::half_size_border_conditions(idx, this);
                }
            }
    )

#undef STRUCTNAME

#define STRUCTNAME ibrae_transfer_impl_tuned

    ADD_ALG(ibrae_transfer_impl_tuned, STRUCTNAME,
            struct STRUCTNAME : public comp_math::state_itf {
                static const std::string name;
                double d_x;
                double d_y;
                double wind;
                double p;
                double w;
                double s;
                double e;
                double n;
                Eigen::SparseMatrix<double> impl_scheme;
                Eigen::BiCGSTAB<Eigen::SparseMatrix<double>> solver;


                using comp_math::state_itf::state_itf;

                std::string get_name() const override {
                    return STRUCTNAME::name;
                }

                p::dict get_state_dict() const override {
                    auto dict = state_itf::get_state_dict();
                    dict["d_x"] = d_x;
                    dict["d_y"] = d_y;
                    dict["wind"] = wind;
                    return dict;
                }

                std::pair<int COMMA int> idx2xy(int idx) const {
                    return std::pair<int COMMA int>(idx % x_point_number() COMMA idx / x_point_number());
                }

                int xy2idx(int x COMMA int y) const {
                    if(x < 0 or x >= x_point_number()){
                        return -1;
                    }
                    if(y < 0 or y >= x_point_number()){
                        return -1;
                    }
                    return x + y * x_point_number();
                }

                std::vector<double> step() override {
                    auto b = get_impl_scheme_vector();
                    std::cout<<"it: "<<it<<std::endl;
                    for(int i = 0; i < y.size(); ++i){
                        std::cout<<y[i]<<" ";
                    }
                    std::cout<<std::endl;
                    Eigen::VectorXd C = solver.solve(b);
                    if (solver.info() != Eigen::Success) {
                        std::cerr << "solver failed. Likely you will have some rubbish instead of results" << std::endl;
                    }
                    y = std::vector<double>(C.data(), C.data() + y.size());
                    ++it;
                    return y;
                }

                void init(const p::dict &dict_state) override {

                    std::cout << std::fixed << std::setprecision(3);
                    state_itf::init(dict_state);
                    d_x = p::extract<double>(dict_state["d_x"]);
                    d_y = p::extract<double>(dict_state["d_y"]);
                    wind = p::extract<double>(dict_state["wind"]);

                    y = std::vector<double>(x_point_number() * x_point_number() COMMA 0);
                    set_starting_conditions();
                    for(int i = 0; i < y.size(); ++i){
                        y[i] = border_conditions(i);
                    }
                    //these are coefs near C_{i,j} C{i-1, j}, C{i+1, j} and so on. hint w: west, e: east
                    //matrix: (x_point_number()^2, x_point_number()^2)

                    p = 1.0 + 2.0 * d_x * dt / (dx * dx) + 2.0 * d_y * dt / (dx * dx) + dt / dx * wind;
                    w =  - d_x * dt / (dx * dx);
                    e = - d_x * dt / (dx * dx) - dt / dx * wind;
                    n = - d_y * dt / (dx * dx);
                    s = - d_y * dt / (dx * dx);

                    impl_scheme = init_impl_scheme_matrix();
                    solver.compute(impl_scheme);
                    if (solver.info() != Eigen::Success) {
                        std::cerr << "decomposition failed. Likely you will have some rubbish results" << std::endl;
                    }
                    std::cout<<"init "<<it<<": "<<y[0]<<std::endl;
                }

                Eigen::SparseMatrix<double COMMA Eigen::RowMajor>
                init_impl_scheme_matrix() {


                    const auto N = y.size();

                    auto impl_scheme_t = Eigen::SparseMatrix<double COMMA Eigen::RowMajor>(N COMMA N);

                    impl_scheme_t.reserve(Eigen::VectorXi::Constant(N COMMA 5));

                    for (int idx = 0; idx < N; ++idx) {
                        const int i = idx;

                        auto idx_pair = idx2xy(idx);
                        int x = idx_pair.first;
                        int y = idx_pair.second;
                        if (int new_idx = xy2idx(x COMMA y - 1); new_idx >= 0) {
                            impl_scheme_t.insert(new_idx COMMA i) = n;
                        }
                        if (int new_idx = xy2idx(x - 1 COMMA y); new_idx >= 0) {
                            impl_scheme_t.insert(new_idx COMMA i) = w;
                        }

                        impl_scheme_t.insert(i COMMA i) = p;

                        if (int new_idx = xy2idx(x + 1 COMMA y); new_idx >= 0) {
                            impl_scheme_t.insert(new_idx COMMA i) = e;
                        }
                        if (int new_idx = xy2idx(x COMMA y + 1); new_idx >= 0) {
                            impl_scheme_t.insert(new_idx COMMA i) = s;
                        }
                    }
                    impl_scheme_t.makeCompressed();

                    return impl_scheme_t;
                }

                Eigen::VectorXd get_impl_scheme_vector() const {
                    auto result = Eigen::VectorXd(y.size());
                    for (int i = 0; i < y.size(); ++i) {
                        auto idx_pair = idx2xy(i);
                        auto x = idx_pair.first;
                        auto y = idx_pair.second;
                        result(i) = border_conditions(xy2idx(x COMMA y));
                        if(int new_idx = xy2idx(x COMMA y - 1); new_idx < 0){
                            result(i) -= n * border_conditions(new_idx);
                        }
                        if(int new_idx = xy2idx(x - 1 COMMA y); new_idx < 0){
                            result(i) -= w * border_conditions(new_idx);
                        }
                        if(int new_idx = xy2idx(x + 1 COMMA y); new_idx < 0){
                            result(i) -= e * border_conditions(new_idx);
                        }
                        if(int new_idx = xy2idx(x COMMA y + 1); new_idx < 0){
                            result(i) -= s * border_conditions(new_idx);
                        }
                    }
                    return result;
                }

                std::string help() override {
                    return state_itf::help() + "\n" +
                           "d_x: " + std::to_string(d_x) + "\n" +
                           "d_y: " + std::to_string(d_y) + "\n" +
                           "wind: " + std::to_string(wind) + "\n";
                }

                p::tuple get_internal_state() const override {
                    auto internal_state = p::make_tuple(d_x, d_y, wind, p, w, s, e, n);

                    return p::tuple(p::list(state_itf::get_internal_state()) + p::list(internal_state));
                }

                p::tuple get_init_args() const override {
                    return state_itf::get_init_args();
                }

                void set_internal_state(const p::tuple &int_state) override {
                    state_itf::set_internal_state(int_state);
                    d_x = p::extract<double>(int_state[default_state_len]);
                    d_y = p::extract<double>(int_state[default_state_len + 1]);
                    wind = p::extract<double>(int_state[default_state_len + 2]);
                    p = p::extract<double>(int_state[default_state_len + 3]);
                    w = p::extract<double>(int_state[default_state_len + 4]);
                    s = p::extract<double>(int_state[default_state_len + 5]);
                    e = p::extract<double>(int_state[default_state_len + 6]);
                    n = p::extract<double>(int_state[default_state_len + 7]);

                    impl_scheme = init_impl_scheme_matrix();
                    solver.compute(impl_scheme);
                    if (solver.info() != Eigen::Success) {
                        std::cerr << "decomposition failed. Likely you will have some rubbish results" << std::endl;
                    }
                }

                double scheme(int i) const override {
                    return - 1; //it's never used
                }

                double starting_conditions(int idx) const override {
                    return comp_math::default_descriptions::zero_starting_conditions(idx, this);
                }

                double border_conditions(int idx) const override {
                    return comp_math::default_descriptions::half_size_border_conditions(idx, this);
                }
            }
            
    )

#undef STRUCTNAME
